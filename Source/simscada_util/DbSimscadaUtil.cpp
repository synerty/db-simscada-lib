/*
 * DbSimscadaUtil.cpp
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#include "DbSimscadaUtil.h"

#include <stdexcept>

#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/format.hpp>

#include "../simscada/sim/SimProtocol.h"
#include "../simscada/sim/SimObject.h"
#include "../simscada/sim/SimCtrlLink.h"
#include "../simscada/sim/SimCtrlPt.h"
#include "../simscada/sim/SimLinkedPt.h"
#include "../simscada/sim/SimPlant.h"
#include "../simscada/sim/SimPlantelement.h"
#include "../simscada/SimScadaModel.h"

namespace db_simscada
{
  /// ---------------
  /// SimObject Object Type

  ObjectTypeE
  toObjectType(const sim::ObjectPtrT object)
  {
    if (!object.get())
      throw std::runtime_error("db_simscada::toObjectType, object is null");

    const std::string ot = object->objectType();

    if (boost::iequals(ot, "PROTOCOL"))
      return ProtocolObject;

    if (boost::iequals(ot, "PORT"))
      return DataPortObject;

    if (boost::iequals(ot, "CHAN"))
      return ChannelObject;

    if (boost::iequals(ot, "RTU"))
      return RtuObject;

    if (boost::iequals(ot, "GROUP"))
      return DataGroupObject;

    if (boost::iequals(ot, "ANA"))
      return AnalogueObject;

    if (boost::iequals(ot, "DIG"))
      return DigitalObject;

    if (boost::iequals(ot, "CTRL"))
      return ControlObject;

    if (boost::iequals(ot, "ACC"))
      return AccumulatorObject;

    boost::format info("%d is not a valid simSCADA object type string value");
    info % ot;
    throw std::runtime_error(info.str());

  }
  // ----------------------------------------------------------------------------

  std::string
  toObjectType(const ObjectTypeE objectType)
  {
    switch (objectType)
    {
      case DataPortObject:
        return "PORT";

      case ChannelObject:
        return "CHAN";

      case RtuObject:
        return "RTU";

      case DataGroupObject:
        return "GROUP";

      case AnalogueObject:
        return "ANA";

      case AccumulatorObject:
        return "ACC";

      case DigitalObject:
        return "DIG";

      case ControlObject:
        return "CTRL";

      case ProtocolObject:
      case ObjectTypeCount:
      case ObjectTypeUnset:
        break;

    }

    boost::format info("%d is not a valid simSCADA object type enum value");
    info % objectType;
    throw std::runtime_error(info.str());
  }
  // ----------------------------------------------------------------------------

  std::string
  toObjectTypeName(const sim::ObjectPtrT object)
  {
    return toObjectTypeName(toObjectType(object));
  }
  // ----------------------------------------------------------------------------

  std::string
  toObjectTypeName(const ObjectTypeE objectType)
  {
    switch (objectType)
    {
      case ProtocolObject:
        return "Protocol";

      case DataPortObject:
        return "Port";

      case ChannelObject:
        return "Channel";

      case RtuObject:
        return "Rtu";

      case DataGroupObject:
        return "Data Group";

      case AnalogueObject:
        return "Analogue";

      case AccumulatorObject:
        return "Accumulator";

      case DigitalObject:
        return "Digital";

      case ControlObject:
        return "Control";

      case ObjectTypeCount:
      case ObjectTypeUnset:
        break;

    }

    boost::format info("%d is not a valid simSCADA object type enum value");
    info % objectType;
    throw std::runtime_error(info.str());
  }
  // ----------------------------------------------------------------------------

  /// ---------------
  /// CtrlLink State To

  CtrlLinkStateToE
  toCtrlLinkStateTo(const sim::CtrlLinkPtrT ctrlLink)
  {
    if (!ctrlLink.get())
      throw std::runtime_error("db_simscada::toCtrlLinkStateTo, ctrlLink is null");

    const std::string stateTo = ctrlLink->stateTo();

    if (boost::iequals(stateTo, "A"))
      return ToStateA;

    if (boost::iequals(stateTo, "B"))
      return ToStateB;

    if (boost::iequals(stateTo, "C"))
      return ToStateC;

    if (boost::iequals(stateTo, "D"))
      return ToStateD;

    if (boost::iequals(stateTo, "T"))
      return ToggleState;

    if (boost::iequals(stateTo, "V"))
      return ToControlValue;

    boost::format info("%d is not a valid CtrlLink StateTo string value");
    info % stateTo;
    throw std::runtime_error(info.str());

  }
  // ----------------------------------------------------------------------------

  std::string
  toCtrlLinkStateTo(const CtrlLinkStateToE objectType)
  {
    switch (objectType)
    {
      case ToStateA:
        return "A";

      case ToStateB:
        return "B";

      case ToStateC:
        return "C";

      case ToStateD:
        return "D";

      case ToggleState:
        return "T";

      case ToControlValue:
        return "V";

      case ToStateUnset:
        break;
    }

    boost::format info("%d is not a valid CtrlLink StateTo enum value");
    info % objectType;
    throw std::runtime_error(info.str());
  }
  // ----------------------------------------------------------------------------

  std::string
  toCtrlLinkStateToName(const sim::CtrlLinkPtrT ctrlLink)
  {
    return toCtrlLinkStateToName(toCtrlLinkStateTo(ctrlLink));
  }
  // ----------------------------------------------------------------------------

  std::string
  toCtrlLinkStateToName(const CtrlLinkStateToE objectType)
  {
    switch (objectType)
    {
      case ToStateA:
        return "To State A";

      case ToStateB:
        return "To State B";

      case ToStateC:
        return "To State C";

      case ToStateD:
        return "To State D";

      case ToggleState:
        return "Toggle State";

      case ToControlValue:
        return "To Control Value";

      case ToStateUnset:
        break;

    }

    boost::format info("%d is not a valid CtrlLink StateTo enum value");
    info % objectType;
    throw std::runtime_error(info.str());
  }
  // ----------------------------------------------------------------------------

  /// ---------------
  /// Protocol Stuff

  ProtocolE
  toObjectProtocol(sim::ObjectPtrT object)
  {
    const std::string prot = object->protocol()->name();

    if (boost::iequals(prot, "conitel"))
      return conitelProtocol;

    if (boost::iequals(prot, "dnp"))
      return dnpProtocol;

    boost::format info("Protocol type '%s' not recignised");
    info % prot;
    throw std::runtime_error(info.str());

  }
// ----------------------------------------------------------------------------

/// ---------------
/// Control Point stuff

  ControlTypeE
  toCtrlType(sim::CtrlPtPtrT ctrlPoint)
  {
    ProtocolE protocol = toObjectProtocol(ctrlPoint);

    switch (protocol)
    {
      case conitelProtocol:
      {
        const std::string type = ctrlPoint->ctrlDef4();

        if (boost::iequals(type, "setpoint"))
          return setpointCtrlType;

        if (boost::iequals(type, "tripclose"))
          return digitalCtrlType;

        if (boost::iequals(type, "UNITRALO"))
          return raiseLowerCtrlType;

        boost::format info("Protocol '%s', control type '%s' not recignised");
        info % protocol;
        info % type;
        throw std::runtime_error(info.str());

        break;
      }

      case dnpProtocol:
      {
        const std::string type = ctrlPoint->ctrlType();

        if (boost::iequals(type, "SETPOINT"))
          return setpointCtrlType;

        if (boost::iequals(type, "DIG"))
          return digitalCtrlType;

        if (boost::iequals(type, "RAISELOWER"))
          return raiseLowerCtrlType;

        boost::format info("Protocol '%s', control type '%s' not recignised");
        info % protocol;
        info % type;
        throw std::runtime_error(info.str());

        break;
      }

      case unknownProtocol:
        break;
    }

    boost::format info("Protocol type %d '%s' not handled");
    info % protocol;
    info % ctrlPoint->protocol()->name();
    throw std::runtime_error(info.str());
  }
// ----------------------------------------------------------------------------

/// ---------------
/// Setup Model stuff

  void
  setupModel(SimScadaModelPtrT model)
  {
    sim::ProtocolPtrT nullProt(new sim::Protocol());

    sim::ObjectPtrT dnp(new sim::Object());
    dnp->config()->state().setExcludeFromDb(true);
    dnp->setAlias("DNP");
    dnp->setName("DNP");
    dnp->setObjectType("Protocol");
    dnp->setProtocol(nullProt);
    dnp->addToModel(model);
    dnp->setProtocolNull(true);

    sim::ObjectPtrT dnpDual(new sim::Object());
    dnpDual->config()->state().setExcludeFromDb(true);
    dnpDual->setAlias("DNP_dual");
    dnpDual->setName("DNP");
    dnpDual->setObjectType("Protocol");
    dnpDual->setProtocol(nullProt);
    dnpDual->addToModel(model);
    dnpDual->setProtocolNull(true);

    sim::ObjectPtrT conitel(new sim::Object());
    conitel->config()->state().setExcludeFromDb(true);
    conitel->setAlias("Conitel");
    conitel->setName("Conitel");
    conitel->setObjectType("Protocol");
    conitel->setProtocol(nullProt);
    conitel->addToModel(model);
    conitel->setProtocolNull(true);

    sim::ObjectPtrT conitelUpper(new sim::Object());
    conitelUpper->config()->state().setExcludeFromDb(true);
    conitelUpper->setAlias("CONITEL");
    conitelUpper->setName("CONITEL");
    conitelUpper->setObjectType("Protocol");
    conitelUpper->setProtocol(nullProt);
    conitelUpper->addToModel(model);
    conitelUpper->setProtocolNull(true);

    sim::ObjectPtrT conitelDual(new sim::Object());
    conitelDual->config()->state().setExcludeFromDb(true);
    conitelDual->setAlias("Conitel_dual");
    conitelDual->setName("Conitel");
    conitelDual->setObjectType("Protocol");
    conitelDual->setProtocol(nullProt);
    conitelDual->addToModel(model);
    conitelDual->setProtocolNull(true);

    sim::ObjectPtrT conitelDualUpper(new sim::Object());
    conitelDualUpper->config()->state().setExcludeFromDb(true);
    conitelDualUpper->setAlias("CONITEL_dual");
    conitelDualUpper->setName("CONITEL");
    conitelDualUpper->setObjectType("Protocol");
    conitelDualUpper->setProtocol(nullProt);
    conitelDualUpper->addToModel(model);
    conitelDualUpper->setProtocolNull(true);

  }
/// -------------------------------------------------------------------------

  void
  cleanModel(SimScadaModelPtrT model)
  {
    {
      // Remove all the simObjects
      typedef db_simscada::sim::ObjectListT ListT;
      ListT objs = model->SimObjects();
      for (ListT::iterator itr = objs.begin(); itr != objs.end(); ++itr)
        (*itr)->removeFromModel();
    }

    {
      // Remove all the ctrlLinks
      typedef db_simscada::sim::CtrlLinkListT ListT;
      ListT objs = model->SimCtrlLinks();
      for (ListT::iterator itr = objs.begin(); itr != objs.end(); ++itr)
        (*itr)->removeFromModel();
    }

    {
      // Remove all the Linked pts
      typedef db_simscada::sim::LinkedPtListT ListT;
      ListT objs = model->SimLinkedPts();
      for (ListT::iterator itr = objs.begin(); itr != objs.end(); ++itr)
        (*itr)->removeFromModel();
    }

    {
      // Remove all the Plants
      typedef db_simscada::sim::PlantListT ListT;
      ListT objs = model->SimPlants();
      for (ListT::iterator itr = objs.begin(); itr != objs.end(); ++itr)
        (*itr)->removeFromModel();
    }

    {
      // Remove all the Plant Elements
      typedef db_simscada::sim::PlantElementListT ListT;
      ListT objs = model->SimPlantElements();
      for (ListT::iterator itr = objs.begin(); itr != objs.end(); ++itr)
        (*itr)->removeFromModel();
    }
  }
/// -------------------------------------------------------------------------

  std::string
  aliasise(std::string alias)
  {
    boost::replace_all(alias, " ", "_");
    boost::replace_all(alias, ",", "");
    boost::replace_all(alias, "=", "");
    boost::replace_all(alias, ".", "");
    boost::replace_all(alias, "/", "");
    boost::replace_all(alias, "\\", "");
    boost::replace_all(alias, "-", "");
    boost::replace_all(alias, "+", "");
    boost::replace_all(alias, "(", "");
    boost::replace_all(alias, ")", "");

    boost::to_upper(alias);

    return alias;
  }
/// -------------------------------------------------------------------------

  std::string
  uniquAliasise(std::string alias, SimScadaModelPtrT model)
  {
    const std::string startAlias = aliasise(alias);
    std::string uniqueAlias = startAlias;
    for (int uid = 0; model->SimObject(uniqueAlias).get() != NULL; ++uid)
    {
      boost::format fmt("%s.%d");
      fmt % startAlias;
      fmt % uid;
      uniqueAlias = fmt.str();
    }

    return uniqueAlias;
  }
/// -------------------------------------------------------------------------

}
