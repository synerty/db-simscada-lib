/*
 * DbSimscadaUtil.h
 *
 *  Copyright NG SCADA Systems Pty Ltd 2011
 *
 *  This software is proprietary, you are not free to copy
 *  or redistribute this code in any format.
 *
 *  All rights to this software are reserved by 
 *  NG SCADA Systems Pty Ltd
 */

#ifndef DBSIMSCADAUTIL_H_
#define DBSIMSCADAUTIL_H_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR
#endif

#include <string>

#include "../simscada/DbSimscadaDeclaration.h"

namespace db_simscada
{
  enum ObjectTypeE
  {
    ProtocolObject,
    DataPortObject,
    ChannelObject,
    RtuObject,
    DataGroupObject,
    AnalogueObject,
    AccumulatorObject,
    DigitalObject,
    ControlObject,
    ObjectTypeCount,
    ObjectTypeUnset = 0xFFFF

  };

  /// ---------------
  /// SimObject Object Type

  ObjectTypeE DECL_DB_SIMSCADA_DIR
  toObjectType(const sim::ObjectPtrT object);

  std::string DECL_DB_SIMSCADA_DIR
  toObjectType(const ObjectTypeE objectType);

  std::string DECL_DB_SIMSCADA_DIR
  toObjectTypeName(const sim::ObjectPtrT object);

  std::string DECL_DB_SIMSCADA_DIR
  toObjectTypeName(const ObjectTypeE objectType);

  /// ---------------
  /// CtrlLink State To

  enum CtrlLinkStateToE
  {
    ToStateA,
    ToStateB,
    ToStateC,
    ToStateD,
    ToggleState,
    ToControlValue,
    ToStateUnset
  };

  CtrlLinkStateToE DECL_DB_SIMSCADA_DIR
  toCtrlLinkStateTo(const sim::CtrlLinkPtrT ctrlLink);

  std::string DECL_DB_SIMSCADA_DIR
  toCtrlLinkStateTo(const CtrlLinkStateToE objectType);

  std::string DECL_DB_SIMSCADA_DIR
  toCtrlLinkStateToName(const sim::CtrlLinkPtrT ctrlLink);

  std::string DECL_DB_SIMSCADA_DIR
  toCtrlLinkStateToName(const CtrlLinkStateToE objectType);

  /// ---------------
  /// Protocol Stuff

  enum ProtocolE
  {
    conitelProtocol,
    dnpProtocol,
    unknownProtocol
  };

  ProtocolE DECL_DB_SIMSCADA_DIR
  toObjectProtocol(sim::ObjectPtrT object);

  /// ---------------
  /// Control Point stuff

  enum ControlTypeE
  {
    digitalCtrlType,
    setpointCtrlType,
    raiseLowerCtrlType
  };

  ControlTypeE DECL_DB_SIMSCADA_DIR
  toCtrlType(sim::CtrlPtPtrT ctrlPoint);

  /// ---------------
  /// Setup Model stuff

  void DECL_DB_SIMSCADA_DIR
  setupModel(SimScadaModelPtrT model);

  void DECL_DB_SIMSCADA_DIR
  cleanModel(SimScadaModelPtrT model);

  std::string DECL_DB_SIMSCADA_DIR
  aliasise(std::string alias);

  std::string DECL_DB_SIMSCADA_DIR
  uniquAliasise(std::string alias, SimScadaModelPtrT model);

}

#endif /* DBSIMSCADAUTIL_H_ */
