#ifndef _DB_SIMSCADA_MODEL_H_
#define _DB_SIMSCADA_MODEL_H_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include "orm/v1/Model.h"

#include "DbSimscadaDeclaration.h"

namespace db_simscada {

class DECL_DB_SIMSCADA_DIR SimScadaModel
    : public orm::v1::Model< SimScadaModel>
{
  public:
    SimScadaModel(const orm::v1::Conn& ormConn = orm::v1::Conn());
    virtual ~SimScadaModel();

  public:
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Concentrator, SimConcentrator, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Channel, SimChannel, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::CtrlLink, SimCtrlLink, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Modem, SimModem, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::CtrlDeviceType, SimCtrlDeviceType, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::ModemPool, SimModemPool, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::ModemAttribute, SimModemAttribute, db_simscada::sim::ModemAttributePk)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::IpAddress, SimIpAddress, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DigDeviceType, SimDigDeviceType, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DigClass, SimDigClass, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Plant, SimPlant, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::PlantElement, SimPlantElement, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Protocol, SimProtocol, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Object, SimObject, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Param, SimParam, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::AnaProfile, SimAnaProfile, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::AnaDeviceType, SimAnaDeviceType, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::AnaPt, SimAnaPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::AccPt, SimAccPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::LinkedPt, SimLinkedPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::TelePt, SimTelePt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::StrPt, SimStrPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::Rtu, SimRtu, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DigPt, SimDigPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DataPort, SimDataPort, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DataGroup, SimDataGroup, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DdnBlock, SimDdnBlock, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::CtrlPt, SimCtrlPt, std::string)
    ORM_V1_MODEL_DECLARE_STORABLE(db_simscada::sim::DragDropLookup, SimDragDropLookup, std::string)
};

}

#endif /* _DB_SIMSCADA_MODEL_H_ */
