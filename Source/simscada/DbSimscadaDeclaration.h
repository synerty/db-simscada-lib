#ifndef _DB_SIMSCADA_OBJECT_DECLARATIONS_H_
#define _DB_SIMSCADA_OBJECT_DECLARATIONS_H_

#include <vector>
#include <boost/shared_ptr.hpp>

namespace db_simscada {

  class SimScadaModel;
  typedef boost::shared_ptr< SimScadaModel > SimScadaModelPtrT;
  typedef std::vector< SimScadaModelPtrT > SimScadaModelListT;


namespace sim {
  class DragDropLookup;
  typedef boost::shared_ptr< DragDropLookup > DragDropLookupPtrT;
  typedef std::vector< DragDropLookupPtrT > DragDropLookupListT;

  class CtrlPt;
  typedef boost::shared_ptr< CtrlPt > CtrlPtPtrT;
  typedef std::vector< CtrlPtPtrT > CtrlPtListT;

  class DdnBlock;
  typedef boost::shared_ptr< DdnBlock > DdnBlockPtrT;
  typedef std::vector< DdnBlockPtrT > DdnBlockListT;

  class DataGroup;
  typedef boost::shared_ptr< DataGroup > DataGroupPtrT;
  typedef std::vector< DataGroupPtrT > DataGroupListT;

  class DataPort;
  typedef boost::shared_ptr< DataPort > DataPortPtrT;
  typedef std::vector< DataPortPtrT > DataPortListT;

  class DigPt;
  typedef boost::shared_ptr< DigPt > DigPtPtrT;
  typedef std::vector< DigPtPtrT > DigPtListT;

  class Rtu;
  typedef boost::shared_ptr< Rtu > RtuPtrT;
  typedef std::vector< RtuPtrT > RtuListT;

  class StrPt;
  typedef boost::shared_ptr< StrPt > StrPtPtrT;
  typedef std::vector< StrPtPtrT > StrPtListT;

  class TelePt;
  typedef boost::shared_ptr< TelePt > TelePtPtrT;
  typedef std::vector< TelePtPtrT > TelePtListT;

  class LinkedPt;
  typedef boost::shared_ptr< LinkedPt > LinkedPtPtrT;
  typedef std::vector< LinkedPtPtrT > LinkedPtListT;

  class AccPt;
  typedef boost::shared_ptr< AccPt > AccPtPtrT;
  typedef std::vector< AccPtPtrT > AccPtListT;

  class AnaPt;
  typedef boost::shared_ptr< AnaPt > AnaPtPtrT;
  typedef std::vector< AnaPtPtrT > AnaPtListT;

  class AnaDeviceType;
  typedef boost::shared_ptr< AnaDeviceType > AnaDeviceTypePtrT;
  typedef std::vector< AnaDeviceTypePtrT > AnaDeviceTypeListT;

  class AnaProfile;
  typedef boost::shared_ptr< AnaProfile > AnaProfilePtrT;
  typedef std::vector< AnaProfilePtrT > AnaProfileListT;

  class Param;
  typedef boost::shared_ptr< Param > ParamPtrT;
  typedef std::vector< ParamPtrT > ParamListT;

  class Object;
  typedef boost::shared_ptr< Object > ObjectPtrT;
  typedef std::vector< ObjectPtrT > ObjectListT;

  class Protocol;
  typedef boost::shared_ptr< Protocol > ProtocolPtrT;
  typedef std::vector< ProtocolPtrT > ProtocolListT;

  class PlantElement;
  typedef boost::shared_ptr< PlantElement > PlantElementPtrT;
  typedef std::vector< PlantElementPtrT > PlantElementListT;

  class Plant;
  typedef boost::shared_ptr< Plant > PlantPtrT;
  typedef std::vector< PlantPtrT > PlantListT;

  class DigClass;
  typedef boost::shared_ptr< DigClass > DigClassPtrT;
  typedef std::vector< DigClassPtrT > DigClassListT;

  class DigDeviceType;
  typedef boost::shared_ptr< DigDeviceType > DigDeviceTypePtrT;
  typedef std::vector< DigDeviceTypePtrT > DigDeviceTypeListT;

  class IpAddress;
  typedef boost::shared_ptr< IpAddress > IpAddressPtrT;
  typedef std::vector< IpAddressPtrT > IpAddressListT;

  struct ModemAttributePk;
  class ModemAttribute;
  typedef boost::shared_ptr< ModemAttribute > ModemAttributePtrT;
  typedef std::vector< ModemAttributePtrT > ModemAttributeListT;

  class ModemPool;
  typedef boost::shared_ptr< ModemPool > ModemPoolPtrT;
  typedef std::vector< ModemPoolPtrT > ModemPoolListT;

  class CtrlDeviceType;
  typedef boost::shared_ptr< CtrlDeviceType > CtrlDeviceTypePtrT;
  typedef std::vector< CtrlDeviceTypePtrT > CtrlDeviceTypeListT;

  class Modem;
  typedef boost::shared_ptr< Modem > ModemPtrT;
  typedef std::vector< ModemPtrT > ModemListT;

  class CtrlLink;
  typedef boost::shared_ptr< CtrlLink > CtrlLinkPtrT;
  typedef std::vector< CtrlLinkPtrT > CtrlLinkListT;

  class Channel;
  typedef boost::shared_ptr< Channel > ChannelPtrT;
  typedef std::vector< ChannelPtrT > ChannelListT;

  class Concentrator;
  typedef boost::shared_ptr< Concentrator > ConcentratorPtrT;
  typedef std::vector< ConcentratorPtrT > ConcentratorListT;

}


}

#endif /* _DB_SIMSCADA_OBJECT_DECLARATIONS_H_ */
