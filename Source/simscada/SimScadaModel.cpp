#include "SimScadaModel.h"

#include <orm/v1/ModelWrapper.ini>

// Add explicit instantiations for the model wrapper
template class orm::v1::Model< db_simscada::SimScadaModel >;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Concentrator>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Channel>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlLink>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Modem>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlDeviceType>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemPool>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemAttribute>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::IpAddress>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigDeviceType>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigClass>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Plant>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::PlantElement>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Protocol>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Object>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Param>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaProfile>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaDeviceType>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::AccPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::LinkedPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::TelePt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::StrPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::Rtu>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataPort>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataGroup>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DdnBlock>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlPt>;
template class orm::v1::ModelWrapper<db_simscada::SimScadaModel, db_simscada::sim::DragDropLookup>;

namespace db_simscada {

SimScadaModel::SimScadaModel(const orm::v1::Conn& ormConn)
  : BaseModel(ormConn)
{
  addStorageWrapper(mSimConcentratorStorageWrapper);
  addStorageWrapper(mSimChannelStorageWrapper);
  addStorageWrapper(mSimCtrlLinkStorageWrapper);
  addStorageWrapper(mSimModemStorageWrapper);
  addStorageWrapper(mSimCtrlDeviceTypeStorageWrapper);
  addStorageWrapper(mSimModemPoolStorageWrapper);
  addStorageWrapper(mSimModemAttributeStorageWrapper);
  addStorageWrapper(mSimIpAddressStorageWrapper);
  addStorageWrapper(mSimDigDeviceTypeStorageWrapper);
  addStorageWrapper(mSimDigClassStorageWrapper);
  addStorageWrapper(mSimPlantStorageWrapper);
  addStorageWrapper(mSimPlantElementStorageWrapper);
  addStorageWrapper(mSimProtocolStorageWrapper);
  addStorageWrapper(mSimObjectStorageWrapper);
  addStorageWrapper(mSimParamStorageWrapper);
  addStorageWrapper(mSimAnaProfileStorageWrapper);
  addStorageWrapper(mSimAnaDeviceTypeStorageWrapper);
  addStorageWrapper(mSimAnaPtStorageWrapper);
  addStorageWrapper(mSimAccPtStorageWrapper);
  addStorageWrapper(mSimLinkedPtStorageWrapper);
  addStorageWrapper(mSimTelePtStorageWrapper);
  addStorageWrapper(mSimStrPtStorageWrapper);
  addStorageWrapper(mSimRtuStorageWrapper);
  addStorageWrapper(mSimDigPtStorageWrapper);
  addStorageWrapper(mSimDataPortStorageWrapper);
  addStorageWrapper(mSimDataGroupStorageWrapper);
  addStorageWrapper(mSimDdnBlockStorageWrapper);
  addStorageWrapper(mSimCtrlPtStorageWrapper);
  addStorageWrapper(mSimDragDropLookupStorageWrapper);
}
// ----------------------------------------------------------------------------

SimScadaModel::~SimScadaModel()
{
}
// ----------------------------------------------------------------------------

    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Concentrator, SimConcentrator, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Channel, SimChannel, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::CtrlLink, SimCtrlLink, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Modem, SimModem, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::CtrlDeviceType, SimCtrlDeviceType, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::ModemPool, SimModemPool, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::ModemAttribute, SimModemAttribute, db_simscada::sim::ModemAttributePk)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::IpAddress, SimIpAddress, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DigDeviceType, SimDigDeviceType, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DigClass, SimDigClass, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Plant, SimPlant, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::PlantElement, SimPlantElement, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Protocol, SimProtocol, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Object, SimObject, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Param, SimParam, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::AnaProfile, SimAnaProfile, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::AnaDeviceType, SimAnaDeviceType, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::AnaPt, SimAnaPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::AccPt, SimAccPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::LinkedPt, SimLinkedPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::TelePt, SimTelePt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::StrPt, SimStrPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::Rtu, SimRtu, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DigPt, SimDigPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DataPort, SimDataPort, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DataGroup, SimDataGroup, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DdnBlock, SimDdnBlock, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::CtrlPt, SimCtrlPt, std::string)
    ORM_V1_MODEL_DEFINE_STORABLE(SimScadaModel, db_simscada::sim::DragDropLookup, SimDragDropLookup, std::string)

}
