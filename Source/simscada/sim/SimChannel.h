#ifndef _SIM_CHANNEL_
#define _SIM_CHANNEL_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Channel;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Channel, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Channel
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Channel , db_simscada::sim::Object>
{
  public:
    Channel();
    virtual ~Channel();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(poa, Poa, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(channelNumber, ChannelNumber, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(masterIp, MasterIp, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(masterPort, MasterPort, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(phoneNumber, PhoneNumber, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(protocolVariant, ProtocolVariant, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(chAux1, ChAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(chAux2, ChAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(chAux3, ChAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(chAux4, ChAux4, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(msgSource, MsgSource, std::string)
};

}
}

#endif /* _SIM_CHANNEL_ */
