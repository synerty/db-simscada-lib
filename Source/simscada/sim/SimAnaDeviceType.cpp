#include "SimAnaDeviceType.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AnaDeviceType, std::string, id) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaDeviceType, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaDeviceType>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaDeviceType, id, Id, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::AnaDeviceType, anaPtDeviceTypes, AnaPtDeviceTypes, db_simscada::sim::AnaPt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaDeviceType, description, Description, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(AnaDeviceType)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

AnaDeviceType::AnaDeviceType()
{
  mPropId.setup(this);
  mPropDescription.setup(this);

  // Related back references.
  mPropAnaPtDeviceTypes.setup(this);
}
// ----------------------------------------------------------------------------

AnaDeviceType::~AnaDeviceType()
{
}
// ----------------------------------------------------------------------------

bool
AnaDeviceType::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimAnaDeviceType;
  sModelRemoveFunction = &ModelT::removeSimAnaDeviceType;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::AnaDeviceType > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ana Device Type");
  CfgT::setSqlOpts()->setTableName("tblAnaDeviceTypes");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DescriptionPropT::setupStatic(
    "Description",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
