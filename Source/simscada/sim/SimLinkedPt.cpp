#include "SimLinkedPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "../sim/SimTelePt.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::LinkedPt, std::string, pointAlias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::LinkedPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::LinkedPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::LinkedPt, pointAlias, PointAlias, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::LinkedPt, linkedAlias, LinkedAlias, std::string, db_simscada::sim::TelePt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::LinkedPt, linkType, LinkType, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(LinkedPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

LinkedPt::LinkedPt()
{
  mPropPointAlias.setup(this);
  mPropLinkedAlias.setup(this);
  mPropLinkType.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

LinkedPt::~LinkedPt()
{
}
// ----------------------------------------------------------------------------

bool
LinkedPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimLinkedPt;
  sModelRemoveFunction = &ModelT::removeSimLinkedPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::LinkedPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Linked Pt");
  CfgT::setSqlOpts()->setTableName("tblLinkedPts");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(PointAliasPropT::setupStatic(
    "PointAlias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(LinkedAliasPropT::setupStatic(
    "LinkedAlias",
    &db_simscada::sim::TelePt::alias,
    &SimScadaModel::SimTelePt,
    &db_simscada::sim::TelePt::LinkedPtLinkedAliass,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(LinkTypePropT::setupStatic(
    "LinkType",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
