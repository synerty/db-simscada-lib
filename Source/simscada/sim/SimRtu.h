#ifndef _SIM_RTU_
#define _SIM_RTU_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Rtu;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Rtu, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Rtu
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Rtu , db_simscada::sim::Object>
{
  public:
    Rtu();
    virtual ~Rtu();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr1, Addr1, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr2, Addr2, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(delay, Delay, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dialInMode, DialInMode, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(phoneNumber, PhoneNumber, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(protocolVariant, ProtocolVariant, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rbemode, Rbemode, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(retries, Retries, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(startOnline, StartOnline, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(timeout, Timeout, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rtAux1, RtAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rtAux2, RtAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rtAux3, RtAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rtAux4, RtAux4, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(commissioningState, CommissioningState, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(initialTime, InitialTime, int32_t)
};

}
}

#endif /* _SIM_RTU_ */
