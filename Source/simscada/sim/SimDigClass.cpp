#include "SimDigClass.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DigClass, std::string, id) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigClass, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigClass>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, stateBText, StateBText, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, stateAText, StateAText, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, description, Description, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, stateCText, StateCText, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, stateDText, StateDText, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigClass, id, Id, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::DigClass, digPtDeviceClasss, DigPtDeviceClasss, db_simscada::sim::DigPt)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DigClass)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DigClass::DigClass()
{
  mPropStateBText.setup(this);
  mPropStateAText.setup(this);
  mPropDescription.setup(this);
  mPropStateCText.setup(this);
  mPropStateDText.setup(this);
  mPropId.setup(this);

  // Related back references.
  mPropDigPtDeviceClasss.setup(this);
}
// ----------------------------------------------------------------------------

DigClass::~DigClass()
{
}
// ----------------------------------------------------------------------------

bool
DigClass::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDigClass;
  sModelRemoveFunction = &ModelT::removeSimDigClass;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DigClass > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Dig Class");
  CfgT::setSqlOpts()->setTableName("tblDigClasses");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(StateBTextPropT::setupStatic(
    "StateBText",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateATextPropT::setupStatic(
    "StateAText",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DescriptionPropT::setupStatic(
    "Description",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateCTextPropT::setupStatic(
    "StateCText",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateDTextPropT::setupStatic(
    "StateDText",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
