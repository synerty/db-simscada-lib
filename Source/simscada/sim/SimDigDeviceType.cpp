#include "SimDigDeviceType.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DigDeviceType, std::string, id) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigDeviceType, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigDeviceType>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, description, Description, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, stateC, StateC, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, stateD, StateD, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, doubleBit, DoubleBit, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, id, Id, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::DigDeviceType, digPtDeviceTypes, DigPtDeviceTypes, db_simscada::sim::DigPt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, stateA, StateA, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigDeviceType, stateB, StateB, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DigDeviceType)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DigDeviceType::DigDeviceType()
{
  mPropDescription.setup(this);
  mPropStateC.setup(this);
  mPropStateD.setup(this);
  mPropDoubleBit.setup(this);
  mPropId.setup(this);
  mPropStateA.setup(this);
  mPropStateB.setup(this);

  // Related back references.
  mPropDigPtDeviceTypes.setup(this);
}
// ----------------------------------------------------------------------------

DigDeviceType::~DigDeviceType()
{
}
// ----------------------------------------------------------------------------

bool
DigDeviceType::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDigDeviceType;
  sModelRemoveFunction = &ModelT::removeSimDigDeviceType;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DigDeviceType > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Dig Device Type");
  CfgT::setSqlOpts()->setTableName("tblDigDeviceTypes");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DescriptionPropT::setupStatic(
    "Description",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateCPropT::setupStatic(
    "StateC",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(StateDPropT::setupStatic(
    "StateD",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(DoubleBitPropT::setupStatic(
    "DoubleBit",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateAPropT::setupStatic(
    "StateA",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(StateBPropT::setupStatic(
    "StateB",
    false,
    false,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
