#include "SimModem.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Modem, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Modem, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Modem>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Modem, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Modem, typeId, TypeId, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Modem, modemPoolAlias, ModemPoolAlias, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Modem)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Modem::Modem()
{
  mPropAlias.setup(this);
  mPropTypeId.setup(this);
  mPropModemPoolAlias.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

Modem::~Modem()
{
}
// ----------------------------------------------------------------------------

bool
Modem::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimModem;
  sModelRemoveFunction = &ModelT::removeSimModem;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Modem > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Modem");
  CfgT::setSqlOpts()->setTableName("tblModems");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TypeIdPropT::setupStatic(
    "TypeID",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ModemPoolAliasPropT::setupStatic(
    "ModemPoolAlias",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
