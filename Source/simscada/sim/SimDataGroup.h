#ifndef _SIM_DATA_GROUP_
#define _SIM_DATA_GROUP_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DataGroup;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DataGroup, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DataGroup
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DataGroup , db_simscada::sim::Object>
{
  public:
    DataGroup();
    virtual ~DataGroup();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(type, Type, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(address, Address, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dgAux1, DgAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dgAux2, DgAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dgAux3, DgAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dgAux4, DgAux4, std::string)
};

}
}

#endif /* _SIM_DATA_GROUP_ */
