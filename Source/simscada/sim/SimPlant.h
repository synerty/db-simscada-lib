#ifndef _SIM_PLANT_
#define _SIM_PLANT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Plant;
  class PlantElement;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Plant, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Plant
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Plant >
{
  public:
    Plant();
    virtual ~Plant();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(plantElementPlants, PlantElementPlants, db_simscada::sim::PlantElement)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(plantType, PlantType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(parent, Parent, std::string)
};

}
}

#endif /* _SIM_PLANT_ */
