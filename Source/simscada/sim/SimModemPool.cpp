#include "SimModemPool.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::ModemPool, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemPool, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemPool>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::ModemPool, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::ModemPool, modemSelectionScheme, ModemSelectionScheme, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(ModemPool)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

ModemPool::ModemPool()
{
  mPropAlias.setup(this);
  mPropModemSelectionScheme.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

ModemPool::~ModemPool()
{
}
// ----------------------------------------------------------------------------

bool
ModemPool::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimModemPool;
  sModelRemoveFunction = &ModelT::removeSimModemPool;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::ModemPool > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Modem Pool");
  CfgT::setSqlOpts()->setTableName("tblModemPools");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ModemSelectionSchemePropT::setupStatic(
    "ModemSelectionScheme",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
