#include "SimDdnBlock.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DdnBlock, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DdnBlock, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DdnBlock>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::DdnBlock, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, blockNumber, BlockNumber, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, channelAlias, ChannelAlias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, type, Type, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, cyclePeriod, CyclePeriod, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, cycleSend, CycleSend, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, spontSend, SpontSend, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DdnBlock, wordCount, WordCount, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DdnBlock)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DdnBlock::DdnBlock()
{
  mPropAlias.setup(this);
  mPropBlockNumber.setup(this);
  mPropChannelAlias.setup(this);
  mPropType.setup(this);
  mPropCyclePeriod.setup(this);
  mPropCycleSend.setup(this);
  mPropSpontSend.setup(this);
  mPropWordCount.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

DdnBlock::~DdnBlock()
{
}
// ----------------------------------------------------------------------------

bool
DdnBlock::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDdnBlock;
  sModelRemoveFunction = &ModelT::removeSimDdnBlock;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DdnBlock > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ddn Block");
  CfgT::setSqlOpts()->setTableName("tblDDNBlocks");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(BlockNumberPropT::setupStatic(
    "BlockNumber",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ChannelAliasPropT::setupStatic(
    "ChannelAlias",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TypePropT::setupStatic(
    "Type",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CyclePeriodPropT::setupStatic(
    "CyclePeriod",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CycleSendPropT::setupStatic(
    "CycleSend",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SpontSendPropT::setupStatic(
    "SpontSend",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(WordCountPropT::setupStatic(
    "WordCount",
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
