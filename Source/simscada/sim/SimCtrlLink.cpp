#include "SimCtrlLink.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "../sim/SimTelePt.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::CtrlLink, std::string, ctrlPoint) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlLink, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlLink>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::CtrlLink, scanPoint, ScanPoint, std::string, db_simscada::sim::TelePt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlLink, stateTo, StateTo, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlLink, priority, Priority, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlLink, ctrlPoint, CtrlPoint, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlLink, delay, Delay, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(CtrlLink)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

CtrlLink::CtrlLink()
{
  mPropScanPoint.setup(this);
  mPropStateTo.setup(this);
  mPropPriority.setup(this);
  mPropCtrlPoint.setup(this);
  mPropDelay.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

CtrlLink::~CtrlLink()
{
}
// ----------------------------------------------------------------------------

bool
CtrlLink::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimCtrlLink;
  sModelRemoveFunction = &ModelT::removeSimCtrlLink;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::CtrlLink > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ctrl Link");
  CfgT::setSqlOpts()->setTableName("tblCtrlLinks");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(ScanPointPropT::setupStatic(
    "ScanPoint",
    &db_simscada::sim::TelePt::alias,
    &SimScadaModel::SimTelePt,
    &db_simscada::sim::TelePt::CtrlLinkScanPoints,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StateToPropT::setupStatic(
    "StateTo",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PriorityPropT::setupStatic(
    "Priority",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CtrlPointPropT::setupStatic(
    "CtrlPoint",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DelayPropT::setupStatic(
    "Delay",
    false,
    false,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
