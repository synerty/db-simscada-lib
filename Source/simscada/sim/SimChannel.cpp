#include "SimChannel.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Channel, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Channel, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Channel>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, poa, Poa, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, channelNumber, ChannelNumber, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, masterIp, MasterIp, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, masterPort, MasterPort, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, phoneNumber, PhoneNumber, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, protocolVariant, ProtocolVariant, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, chAux1, ChAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, chAux2, ChAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, chAux3, ChAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, chAux4, ChAux4, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::Channel, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Channel, msgSource, MsgSource, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Channel)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Channel::Channel()
{
  mPropPoa.setup(this);
  mPropChannelNumber.setup(this);
  mPropMasterIp.setup(this);
  mPropMasterPort.setup(this);
  mPropPhoneNumber.setup(this);
  mPropProtocolVariant.setup(this);
  mPropChAux1.setup(this);
  mPropChAux2.setup(this);
  mPropChAux3.setup(this);
  mPropChAux4.setup(this);
  mPropAlias.setup(this);
  mPropMsgSource.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

Channel::~Channel()
{
}
// ----------------------------------------------------------------------------

bool
Channel::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimChannel;
  sModelRemoveFunction = &ModelT::removeSimChannel;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Channel > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Channel");
  CfgT::setSqlOpts()->setTableName("tblChannels");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(PoaPropT::setupStatic(
    "POA",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ChannelNumberPropT::setupStatic(
    "ChannelNumber",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(MasterIpPropT::setupStatic(
    "MasterIP",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(MasterPortPropT::setupStatic(
    "MasterPort",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(PhoneNumberPropT::setupStatic(
    "PhoneNumber",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ProtocolVariantPropT::setupStatic(
    "ProtocolVariant",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ChAux1PropT::setupStatic(
    "chAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ChAux2PropT::setupStatic(
    "chAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ChAux3PropT::setupStatic(
    "chAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ChAux4PropT::setupStatic(
    "chAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(MsgSourcePropT::setupStatic(
    "MsgSource",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
