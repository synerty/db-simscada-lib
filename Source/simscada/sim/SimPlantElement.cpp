#include "SimPlantElement.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "../sim/SimPlant.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::PlantElement, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::PlantElement, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::PlantElement>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, defSimNetRep, DefSimNetRep, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::PlantElement, plant, Plant, std::string, db_simscada::sim::Plant)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, id, Id, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, point, Point, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, roleDesc, RoleDesc, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, elementType, ElementType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, role, Role, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::PlantElement, value, Value, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(PlantElement)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

PlantElement::PlantElement()
{
  mPropAlias.setup(this);
  mPropDefSimNetRep.setup(this);
  mPropPlant.setup(this);
  mPropId.setup(this);
  mPropPoint.setup(this);
  mPropRoleDesc.setup(this);
  mPropElementType.setup(this);
  mPropName.setup(this);
  mPropRole.setup(this);
  mPropValue.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

PlantElement::~PlantElement()
{
}
// ----------------------------------------------------------------------------

bool
PlantElement::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimPlantElement;
  sModelRemoveFunction = &ModelT::removeSimPlantElement;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::PlantElement > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Plant Element");
  CfgT::setSqlOpts()->setTableName("tblPlantElement");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DefSimNetRepPropT::setupStatic(
    "DefSimNetRep",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PlantPropT::setupStatic(
    "Plant",
    &db_simscada::sim::Plant::alias,
    &SimScadaModel::SimPlant,
    &db_simscada::sim::Plant::PlantElementPlants,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PointPropT::setupStatic(
    "Point",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RoleDescPropT::setupStatic(
    "RoleDesc",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ElementTypePropT::setupStatic(
    "ElementType",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "Name",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RolePropT::setupStatic(
    "Role",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ValuePropT::setupStatic(
    "Value",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
