#ifndef _SIM_MODEM_ATTRIBUTE_
#define _SIM_MODEM_ATTRIBUTE_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class ModemAttribute;
  class ModemAttributePk;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::ModemAttribute, db_simscada::sim::ModemAttributePk)

namespace db_simscada {
namespace sim {

struct DECL_DB_SIMSCADA_DIR ModemAttributePk 
{
  ModemAttributePk(const std::string& attributeName_
    , const std::string& modemTypeId_);

  bool operator<(const ModemAttributePk& o) const;

  const std::string attributeName;
  const std::string modemTypeId;
};

class DECL_DB_SIMSCADA_DIR ModemAttribute
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::ModemAttribute >
{
  public:
    ModemAttribute();
    virtual ~ModemAttribute();

  public:
    static bool setupStatic();

  public:
ModemAttributePk primaryKey_() const;

    ORM_V1_DECLARE_SIMPLE_PROPERTY(attributeName, AttributeName, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(modemTypeId, ModemTypeId, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(attributeValue, AttributeValue, std::string)
};

}
}

#endif /* _SIM_MODEM_ATTRIBUTE_ */
