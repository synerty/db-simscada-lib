#ifndef _SIM_OBJECT_
#define _SIM_OBJECT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Object;
  class Protocol;
  class Object;
  class Object;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Object, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Object
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Object >
{
  public:
    Object();
    virtual ~Object();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_RELATED_PROPERTY(parent, Parent, std::string, db_simscada::sim::Object)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(intPassedNum, IntPassedNum, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(objectType, ObjectType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(intFailedNum, IntFailedNum, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(errorDesc, ErrorDesc, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(externalId, ExternalId, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sourceRefVer, SourceRefVer, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(invisible, Invisible, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sourceRefDate, SourceRefDate, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(sourceRef, SourceRef, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(integrityDate, IntegrityDate, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(intResState, IntResState, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(objectParents, ObjectParents, db_simscada::sim::Object)
    ORM_V1_DECLARE_RELATED_PROPERTY(protocol, Protocol, std::string, db_simscada::sim::Protocol)
};

}
}

#endif /* _SIM_OBJECT_ */
