#ifndef _SIM_DDN_BLOCK_
#define _SIM_DDN_BLOCK_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DdnBlock;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DdnBlock, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DdnBlock
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DdnBlock , db_simscada::sim::Object>
{
  public:
    DdnBlock();
    virtual ~DdnBlock();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(blockNumber, BlockNumber, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(channelAlias, ChannelAlias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(type, Type, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cyclePeriod, CyclePeriod, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cycleSend, CycleSend, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(spontSend, SpontSend, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(wordCount, WordCount, int32_t)
};

}
}

#endif /* _SIM_DDN_BLOCK_ */
