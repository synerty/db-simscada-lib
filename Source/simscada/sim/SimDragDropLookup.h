#ifndef _SIM_DRAG_DROP_LOOKUP_
#define _SIM_DRAG_DROP_LOOKUP_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DragDropLookup;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DragDropLookup, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DragDropLookup
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DragDropLookup >
{
  public:
    DragDropLookup();
    virtual ~DragDropLookup();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dragDropText, DragDropText, std::string)
};

}
}

#endif /* _SIM_DRAG_DROP_LOOKUP_ */
