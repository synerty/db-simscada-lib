#ifndef _SIM_LINKED_PT_
#define _SIM_LINKED_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class LinkedPt;
  class TelePt;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::LinkedPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR LinkedPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::LinkedPt >
{
  public:
    LinkedPt();
    virtual ~LinkedPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(pointAlias, PointAlias, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(linkedAlias, LinkedAlias, std::string, db_simscada::sim::TelePt)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(linkType, LinkType, std::string)
};

}
}

#endif /* _SIM_LINKED_PT_ */
