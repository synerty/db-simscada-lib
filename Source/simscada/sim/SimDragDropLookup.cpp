#include "SimDragDropLookup.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DragDropLookup, std::string, dragDropText) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DragDropLookup, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DragDropLookup>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DragDropLookup, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DragDropLookup, dragDropText, DragDropText, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DragDropLookup)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DragDropLookup::DragDropLookup()
{
  mPropAlias.setup(this);
  mPropDragDropText.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

DragDropLookup::~DragDropLookup()
{
}
// ----------------------------------------------------------------------------

bool
DragDropLookup::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDragDropLookup;
  sModelRemoveFunction = &ModelT::removeSimDragDropLookup;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DragDropLookup > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Drag Drop Lookup");
  CfgT::setSqlOpts()->setTableName("tblDragDropLookup");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DragDropTextPropT::setupStatic(
    "DragDropText",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
