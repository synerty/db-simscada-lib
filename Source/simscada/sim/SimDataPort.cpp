#include "SimDataPort.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DataPort, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataPort, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataPort>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::DataPort, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, portNum, PortNum, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, type, Type, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, active, Active, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, baud, Baud, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, ctsflow, Ctsflow, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, coalesceMsgs, CoalesceMsgs, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, dsrflow, Dsrflow, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, dtrctrl, Dtrctrl, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, dataBits, DataBits, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, expectReflection, ExpectReflection, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, halfDuplex, HalfDuplex, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, ipaddr, Ipaddr, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, parity, Parity, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, postTxRtstime, PostTxRtstime, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, preTxRtstime, PreTxRtstime, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, ptmBytes, PtmBytes, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, rtsctrl, Rtsctrl, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, squelch, Squelch, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataPort, stopBits, StopBits, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DataPort)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DataPort::DataPort()
{
  mPropAlias.setup(this);
  mPropPortNum.setup(this);
  mPropType.setup(this);
  mPropActive.setup(this);
  mPropBaud.setup(this);
  mPropCtsflow.setup(this);
  mPropCoalesceMsgs.setup(this);
  mPropDsrflow.setup(this);
  mPropDtrctrl.setup(this);
  mPropDataBits.setup(this);
  mPropExpectReflection.setup(this);
  mPropHalfDuplex.setup(this);
  mPropIpaddr.setup(this);
  mPropParity.setup(this);
  mPropPostTxRtstime.setup(this);
  mPropPreTxRtstime.setup(this);
  mPropPtmBytes.setup(this);
  mPropRtsctrl.setup(this);
  mPropSquelch.setup(this);
  mPropStopBits.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

DataPort::~DataPort()
{
}
// ----------------------------------------------------------------------------

bool
DataPort::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDataPort;
  sModelRemoveFunction = &ModelT::removeSimDataPort;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DataPort > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Data Port");
  CfgT::setSqlOpts()->setTableName("tblDataPorts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PortNumPropT::setupStatic(
    "PortNum",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(TypePropT::setupStatic(
    "Type",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ActivePropT::setupStatic(
    "Active",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(BaudPropT::setupStatic(
    "Baud",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CtsflowPropT::setupStatic(
    "CTSFlow",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CoalesceMsgsPropT::setupStatic(
    "CoalesceMsgs",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DsrflowPropT::setupStatic(
    "DSRFlow",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DtrctrlPropT::setupStatic(
    "DTRCtrl",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DataBitsPropT::setupStatic(
    "DataBits",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ExpectReflectionPropT::setupStatic(
    "ExpectReflection",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(HalfDuplexPropT::setupStatic(
    "HalfDuplex",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IpaddrPropT::setupStatic(
    "IPAddr",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ParityPropT::setupStatic(
    "Parity",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PostTxRtstimePropT::setupStatic(
    "PostTxRTSTime",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(PreTxRtstimePropT::setupStatic(
    "PreTxRTSTime",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(PtmBytesPropT::setupStatic(
    "PtmBytes",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(RtsctrlPropT::setupStatic(
    "RTSCtrl",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SquelchPropT::setupStatic(
    "Squelch",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(StopBitsPropT::setupStatic(
    "StopBits",
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
