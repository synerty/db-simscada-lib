#include "SimParam.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Param, std::string, paramName) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Param, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Param>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Param, paramName, ParamName, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Param, paramValue, ParamValue, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Param, purpose, Purpose, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Param, units, Units, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Param)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Param::Param()
{
  mPropParamName.setup(this);
  mPropParamValue.setup(this);
  mPropPurpose.setup(this);
  mPropUnits.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

Param::~Param()
{
}
// ----------------------------------------------------------------------------

bool
Param::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimParam;
  sModelRemoveFunction = &ModelT::removeSimParam;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Param > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Param");
  CfgT::setSqlOpts()->setTableName("tblSimParams");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(ParamNamePropT::setupStatic(
    "ParamName",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ParamValuePropT::setupStatic(
    "ParamValue",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PurposePropT::setupStatic(
    "Purpose",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(UnitsPropT::setupStatic(
    "Units",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
