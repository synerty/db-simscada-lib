#include "SimTelePt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::TelePt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::TelePt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::TelePt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::TelePt, alias, Alias, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::TelePt, ctrlLinkScanPoints, CtrlLinkScanPoints, db_simscada::sim::CtrlLink)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::TelePt, linkedPtLinkedAliass, LinkedPtLinkedAliass, db_simscada::sim::LinkedPt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr1, Addr1, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr2, Addr2, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr3, Addr3, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr4, Addr4, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr5, Addr5, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, addr6, Addr6, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, opcoffset, Opcoffset, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, opctag, Opctag, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, opcvariantType, OpcvariantType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, tpAux1, TpAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, tpAux2, TpAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, tpAux3, TpAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, tpAux4, TpAux4, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::TelePt, commissioningState, CommissioningState, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(TelePt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

TelePt::TelePt()
{
  mPropAlias.setup(this);
  mPropAddr1.setup(this);
  mPropAddr2.setup(this);
  mPropAddr3.setup(this);
  mPropAddr4.setup(this);
  mPropAddr5.setup(this);
  mPropAddr6.setup(this);
  mPropOpcoffset.setup(this);
  mPropOpctag.setup(this);
  mPropOpcvariantType.setup(this);
  mPropTpAux1.setup(this);
  mPropTpAux2.setup(this);
  mPropTpAux3.setup(this);
  mPropTpAux4.setup(this);
  mPropCommissioningState.setup(this);

  // Related back references.
  mPropCtrlLinkScanPoints.setup(this);
  mPropLinkedPtLinkedAliass.setup(this);
}
// ----------------------------------------------------------------------------

TelePt::~TelePt()
{
}
// ----------------------------------------------------------------------------

bool
TelePt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimTelePt;
  sModelRemoveFunction = &ModelT::removeSimTelePt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::TelePt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Tele Pt");
  CfgT::setSqlOpts()->setTableName("tblTelePts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(Addr1PropT::setupStatic(
    "Addr_1",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(Addr2PropT::setupStatic(
    "Addr_2",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(Addr3PropT::setupStatic(
    "Addr_3",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(Addr4PropT::setupStatic(
    "Addr_4",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(Addr5PropT::setupStatic(
    "Addr_5",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(Addr6PropT::setupStatic(
    "Addr_6",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(OpcoffsetPropT::setupStatic(
    "OPCOffset",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(OpctagPropT::setupStatic(
    "OPCTag",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(OpcvariantTypePropT::setupStatic(
    "OPCVariantType",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TpAux1PropT::setupStatic(
    "tpAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TpAux2PropT::setupStatic(
    "tpAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TpAux3PropT::setupStatic(
    "tpAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TpAux4PropT::setupStatic(
    "tpAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CommissioningStatePropT::setupStatic(
    "CommissioningState",
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
