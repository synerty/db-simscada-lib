#include "SimDigPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"
#include "../sim/SimDigClass.h"
#include "../sim/SimDigDeviceType.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DigPt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DigPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::DigPt, deviceClass, DeviceClass, std::string, db_simscada::sim::DigClass)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::DigPt, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigPt, defaultState, DefaultState, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::DigPt, deviceType, DeviceType, std::string, db_simscada::sim::DigDeviceType)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigPt, dpAux1, DpAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigPt, dpAux2, DpAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigPt, dpAux3, DpAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DigPt, dpAux4, DpAux4, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DigPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DigPt::DigPt()
{
  mPropDeviceClass.setup(this);
  mPropAlias.setup(this);
  mPropDefaultState.setup(this);
  mPropDeviceType.setup(this);
  mPropDpAux1.setup(this);
  mPropDpAux2.setup(this);
  mPropDpAux3.setup(this);
  mPropDpAux4.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

DigPt::~DigPt()
{
}
// ----------------------------------------------------------------------------

bool
DigPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDigPt;
  sModelRemoveFunction = &ModelT::removeSimDigPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DigPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Dig Pt");
  CfgT::setSqlOpts()->setTableName("tblDigPts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DeviceClassPropT::setupStatic(
    "DeviceClass",
    &db_simscada::sim::DigClass::id,
    &SimScadaModel::SimDigClass,
    &db_simscada::sim::DigClass::DigPtDeviceClasss,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DefaultStatePropT::setupStatic(
    "DefaultState",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DeviceTypePropT::setupStatic(
    "DeviceType",
    &db_simscada::sim::DigDeviceType::id,
    &SimScadaModel::SimDigDeviceType,
    &db_simscada::sim::DigDeviceType::DigPtDeviceTypes,
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DpAux1PropT::setupStatic(
    "dpAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DpAux2PropT::setupStatic(
    "dpAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DpAux3PropT::setupStatic(
    "dpAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DpAux4PropT::setupStatic(
    "dpAux_4",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
