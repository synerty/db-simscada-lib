#ifndef _SIM_TELE_PT_
#define _SIM_TELE_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class TelePt;
  class LinkedPt;
  class CtrlLink;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::TelePt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR TelePt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::TelePt , db_simscada::sim::Object>
{
  public:
    TelePt();
    virtual ~TelePt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(ctrlLinkScanPoints, CtrlLinkScanPoints, db_simscada::sim::CtrlLink)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(linkedPtLinkedAliass, LinkedPtLinkedAliass, db_simscada::sim::LinkedPt)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr1, Addr1, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr2, Addr2, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr3, Addr3, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr4, Addr4, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr5, Addr5, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(addr6, Addr6, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(opcoffset, Opcoffset, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(opctag, Opctag, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(opcvariantType, OpcvariantType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(tpAux1, TpAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(tpAux2, TpAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(tpAux3, TpAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(tpAux4, TpAux4, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(commissioningState, CommissioningState, int32_t)
};

}
}

#endif /* _SIM_TELE_PT_ */
