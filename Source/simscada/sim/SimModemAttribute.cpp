#include "SimModemAttribute.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"

std::ostream& operator<<(std::ostream& os, const db_simscada::sim::ModemAttributePk& o)
{
  os 
    << " attributeName=|" << o.attributeName <<'|'
    << " modemTypeId=|" << o.modemTypeId <<'|';
  return os;
}

ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::ModemAttribute, db_simscada::sim::ModemAttributePk, primaryKey_) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemAttribute, db_simscada::sim::ModemAttributePk>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::ModemAttribute>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::ModemAttribute, attributeName, AttributeName, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::ModemAttribute, modemTypeId, ModemTypeId, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::ModemAttribute, attributeValue, AttributeValue, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(ModemAttribute)
// ----------------------------------------------------------------------------

ModemAttributePk::ModemAttributePk(const std::string& attributeName_
    , const std::string& modemTypeId_) :
  attributeName(attributeName_)//
  , modemTypeId(modemTypeId_)
{
}

bool ModemAttributePk::operator<(const ModemAttributePk& o) const
{
  if (attributeName != o.attributeName)
    return attributeName < o.attributeName;

  return modemTypeId < o.modemTypeId;

}

// ----------------------------------------------------------------------------

ModemAttribute::ModemAttribute()
{
  mPropAttributeName.setup(this);
  mPropModemTypeId.setup(this);
  mPropAttributeValue.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

ModemAttribute::~ModemAttribute()
{
}
// ----------------------------------------------------------------------------

bool
ModemAttribute::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimModemAttribute;
  sModelRemoveFunction = &ModelT::removeSimModemAttribute;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::ModemAttribute > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Modem Attribute");
  CfgT::setSqlOpts()->setTableName("tblModemAttributes");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AttributeNamePropT::setupStatic(
    "AttributeName",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ModemTypeIdPropT::setupStatic(
    "ModemTypeID",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AttributeValuePropT::setupStatic(
    "AttributeValue",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------

ModemAttributePk ModemAttribute::primaryKey_() const
{
  return ModemAttributePk(attributeName()
    , modemTypeId());
}
// ----------------------------------------------------------------------------

}
}
