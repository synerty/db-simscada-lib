#include "SimPlant.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Plant, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Plant, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Plant>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Plant, alias, Alias, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::Plant, plantElementPlants, PlantElementPlants, db_simscada::sim::PlantElement)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Plant, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Plant, plantType, PlantType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Plant, id, Id, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Plant, parent, Parent, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Plant)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Plant::Plant()
{
  mPropAlias.setup(this);
  mPropName.setup(this);
  mPropPlantType.setup(this);
  mPropId.setup(this);
  mPropParent.setup(this);

  // Related back references.
  mPropPlantElementPlants.setup(this);
}
// ----------------------------------------------------------------------------

Plant::~Plant()
{
}
// ----------------------------------------------------------------------------

bool
Plant::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimPlant;
  sModelRemoveFunction = &ModelT::removeSimPlant;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Plant > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Plant");
  CfgT::setSqlOpts()->setTableName("tblPlant");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "Name",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PlantTypePropT::setupStatic(
    "PlantType",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ParentPropT::setupStatic(
    "Parent",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
