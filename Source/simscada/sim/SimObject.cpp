#include "SimObject.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "../sim/SimObject.h"
#include "../sim/SimProtocol.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Object, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Object, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Object>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::Object, parent, Parent, std::string, db_simscada::sim::Object)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, intPassedNum, IntPassedNum, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, objectType, ObjectType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, name, Name, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, intFailedNum, IntFailedNum, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, errorDesc, ErrorDesc, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, externalId, ExternalId, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, sourceRefVer, SourceRefVer, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, invisible, Invisible, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, sourceRefDate, SourceRefDate, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, sourceRef, SourceRef, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, integrityDate, IntegrityDate, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, intResState, IntResState, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Object, alias, Alias, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::Object, objectParents, ObjectParents, db_simscada::sim::Object)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::Object, protocol, Protocol, std::string, db_simscada::sim::Protocol)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Object)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Object::Object()
{
  mPropParent.setup(this);
  mPropIntPassedNum.setup(this);
  mPropObjectType.setup(this);
  mPropName.setup(this);
  mPropIntFailedNum.setup(this);
  mPropErrorDesc.setup(this);
  mPropExternalId.setup(this);
  mPropSourceRefVer.setup(this);
  mPropInvisible.setup(this);
  mPropSourceRefDate.setup(this);
  mPropSourceRef.setup(this);
  mPropIntegrityDate.setup(this);
  mPropIntResState.setup(this);
  mPropAlias.setup(this);
  mPropProtocol.setup(this);

  // Related back references.
  mPropObjectParents.setup(this);
}
// ----------------------------------------------------------------------------

Object::~Object()
{
}
// ----------------------------------------------------------------------------

bool
Object::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimObject;
  sModelRemoveFunction = &ModelT::removeSimObject;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Object > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Object");
  CfgT::setSqlOpts()->setTableName("tblSimObjects");

  CfgT::setSqlOpts()->addCondition("1 = 0");

  CfgT::addFieldMeta(ParentPropT::setupStatic(
    "Parent",
    &db_simscada::sim::Object::alias,
    &SimScadaModel::SimObject,
    &db_simscada::sim::Object::ObjectParents,
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IntPassedNumPropT::setupStatic(
    "IntPassedNum",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ObjectTypePropT::setupStatic(
    "ObjectType",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(NamePropT::setupStatic(
    "Name",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IntFailedNumPropT::setupStatic(
    "IntFailedNum",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ErrorDescPropT::setupStatic(
    "ErrorDesc",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ExternalIdPropT::setupStatic(
    "ExternalID",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SourceRefVerPropT::setupStatic(
    "SourceRefVer",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(InvisiblePropT::setupStatic(
    "Invisible",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SourceRefDatePropT::setupStatic(
    "SourceRefDate",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(SourceRefPropT::setupStatic(
    "SourceRef",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IntegrityDatePropT::setupStatic(
    "IntegrityDate",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(IntResStatePropT::setupStatic(
    "IntResState",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ProtocolPropT::setupStatic(
    "Protocol",
    &db_simscada::sim::Protocol::name,
    &SimScadaModel::SimProtocol,
    &db_simscada::sim::Protocol::ObjectProtocols,
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
