#include "SimStrPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::StrPt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::StrPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::StrPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::StrPt, defaultString, DefaultString, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::StrPt, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::StrPt, spAux1, SpAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::StrPt, spAux2, SpAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::StrPt, spAux3, SpAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::StrPt, spAux4, SpAux4, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(StrPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

StrPt::StrPt()
{
  mPropDefaultString.setup(this);
  mPropAlias.setup(this);
  mPropSpAux1.setup(this);
  mPropSpAux2.setup(this);
  mPropSpAux3.setup(this);
  mPropSpAux4.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

StrPt::~StrPt()
{
}
// ----------------------------------------------------------------------------

bool
StrPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimStrPt;
  sModelRemoveFunction = &ModelT::removeSimStrPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::StrPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Str Pt");
  CfgT::setSqlOpts()->setTableName("tblStrPts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DefaultStringPropT::setupStatic(
    "DefaultString",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SpAux1PropT::setupStatic(
    "spAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SpAux2PropT::setupStatic(
    "spAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SpAux3PropT::setupStatic(
    "spAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(SpAux4PropT::setupStatic(
    "spAux_4",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
