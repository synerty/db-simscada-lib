#include "SimIpAddress.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::IpAddress, std::string, ipaddr) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::IpAddress, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::IpAddress>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::IpAddress, subnetMask, SubnetMask, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::IpAddress, ipaddr, Ipaddr, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(IpAddress)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

IpAddress::IpAddress()
{
  mPropSubnetMask.setup(this);
  mPropIpaddr.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

IpAddress::~IpAddress()
{
}
// ----------------------------------------------------------------------------

bool
IpAddress::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimIpAddress;
  sModelRemoveFunction = &ModelT::removeSimIpAddress;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::IpAddress > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ip Address");
  CfgT::setSqlOpts()->setTableName("tblIPAddresses");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(SubnetMaskPropT::setupStatic(
    "SubnetMask",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IpaddrPropT::setupStatic(
    "IPAddr",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
