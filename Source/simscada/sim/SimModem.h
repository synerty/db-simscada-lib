#ifndef _SIM_MODEM_
#define _SIM_MODEM_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Modem;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Modem, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Modem
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Modem >
{
  public:
    Modem();
    virtual ~Modem();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(typeId, TypeId, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(modemPoolAlias, ModemPoolAlias, std::string)
};

}
}

#endif /* _SIM_MODEM_ */
