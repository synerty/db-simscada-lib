#include "SimConcentrator.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Concentrator, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Concentrator, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Concentrator>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::Concentrator, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, address, Address, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, password, Password, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, userName, UserName, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, coAux1, CoAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, coAux2, CoAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, coAux3, CoAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Concentrator, coAux4, CoAux4, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Concentrator)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Concentrator::Concentrator()
{
  mPropAlias.setup(this);
  mPropAddress.setup(this);
  mPropPassword.setup(this);
  mPropUserName.setup(this);
  mPropCoAux1.setup(this);
  mPropCoAux2.setup(this);
  mPropCoAux3.setup(this);
  mPropCoAux4.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

Concentrator::~Concentrator()
{
}
// ----------------------------------------------------------------------------

bool
Concentrator::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimConcentrator;
  sModelRemoveFunction = &ModelT::removeSimConcentrator;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Concentrator > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Concentrator");
  CfgT::setSqlOpts()->setTableName("tblConcentrators");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AddressPropT::setupStatic(
    "Address",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(PasswordPropT::setupStatic(
    "Password",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(UserNamePropT::setupStatic(
    "UserName",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CoAux1PropT::setupStatic(
    "coAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CoAux2PropT::setupStatic(
    "coAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CoAux3PropT::setupStatic(
    "coAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CoAux4PropT::setupStatic(
    "coAux_4",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
