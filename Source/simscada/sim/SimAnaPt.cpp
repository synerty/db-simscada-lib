#include "SimAnaPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"
#include "../sim/SimAnaDeviceType.h"
#include "../sim/SimAnaProfile.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AnaPt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, defaultValue, DefaultValue, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, engMax, EngMax, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::AnaPt, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, rawMin, RawMin, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, varyRange, VaryRange, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, engMin, EngMin, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, rawMax, RawMax, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, anaDataType, AnaDataType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, alarmHi, AlarmHi, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, alarmHiHi, AlarmHiHi, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, alarmLo, AlarmLo, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, alarmLoLo, AlarmLoLo, double)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, processLimits, ProcessLimits, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, anAux1, AnAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, anAux2, AnAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, anAux3, AnAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, anAux4, AnAux4, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::AnaPt, deviceType, DeviceType, std::string, db_simscada::sim::AnaDeviceType)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::AnaPt, profileName, ProfileName, std::string, db_simscada::sim::AnaProfile)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaPt, jitter, Jitter, double)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(AnaPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

AnaPt::AnaPt()
{
  mPropDefaultValue.setup(this);
  mPropEngMax.setup(this);
  mPropAlias.setup(this);
  mPropRawMin.setup(this);
  mPropVaryRange.setup(this);
  mPropEngMin.setup(this);
  mPropRawMax.setup(this);
  mPropAnaDataType.setup(this);
  mPropAlarmHi.setup(this);
  mPropAlarmHiHi.setup(this);
  mPropAlarmLo.setup(this);
  mPropAlarmLoLo.setup(this);
  mPropProcessLimits.setup(this);
  mPropAnAux1.setup(this);
  mPropAnAux2.setup(this);
  mPropAnAux3.setup(this);
  mPropAnAux4.setup(this);
  mPropDeviceType.setup(this);
  mPropProfileName.setup(this);
  mPropJitter.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

AnaPt::~AnaPt()
{
}
// ----------------------------------------------------------------------------

bool
AnaPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimAnaPt;
  sModelRemoveFunction = &ModelT::removeSimAnaPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::AnaPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ana Pt");
  CfgT::setSqlOpts()->setTableName("tblAnaPts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DefaultValuePropT::setupStatic(
    "DefaultValue",
    false,
    false,
    boost::optional<double>()));
  CfgT::addFieldMeta(EngMaxPropT::setupStatic(
    "EngMax",
    false,
    false,
    boost::optional<double>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RawMinPropT::setupStatic(
    "RawMin",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(VaryRangePropT::setupStatic(
    "VaryRange",
    false,
    false,
    boost::optional<double>()));
  CfgT::addFieldMeta(EngMinPropT::setupStatic(
    "EngMin",
    false,
    false,
    boost::optional<double>()));
  CfgT::addFieldMeta(RawMaxPropT::setupStatic(
    "RawMax",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AnaDataTypePropT::setupStatic(
    "AnaDataType",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AlarmHiPropT::setupStatic(
    "AlarmHi",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(AlarmHiHiPropT::setupStatic(
    "AlarmHiHi",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(AlarmLoPropT::setupStatic(
    "AlarmLo",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(AlarmLoLoPropT::setupStatic(
    "AlarmLoLo",
    false,
    true,
    boost::optional<double>()));
  CfgT::addFieldMeta(ProcessLimitsPropT::setupStatic(
    "ProcessLimits",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AnAux1PropT::setupStatic(
    "anAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AnAux2PropT::setupStatic(
    "anAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AnAux3PropT::setupStatic(
    "anAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AnAux4PropT::setupStatic(
    "anAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DeviceTypePropT::setupStatic(
    "DeviceType",
    &db_simscada::sim::AnaDeviceType::id,
    &SimScadaModel::SimAnaDeviceType,
    &db_simscada::sim::AnaDeviceType::AnaPtDeviceTypes,
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ProfileNamePropT::setupStatic(
    "ProfileName",
    &db_simscada::sim::AnaProfile::profileName,
    &SimScadaModel::SimAnaProfile,
    &db_simscada::sim::AnaProfile::AnaPtProfileNames,
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(JitterPropT::setupStatic(
    "Jitter",
    false,
    true,
    boost::optional<double>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
