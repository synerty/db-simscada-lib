#ifndef _SIM_CONCENTRATOR_
#define _SIM_CONCENTRATOR_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Concentrator;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Concentrator, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Concentrator
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Concentrator , db_simscada::sim::Object>
{
  public:
    Concentrator();
    virtual ~Concentrator();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(address, Address, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(password, Password, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(userName, UserName, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(coAux1, CoAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(coAux2, CoAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(coAux3, CoAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(coAux4, CoAux4, std::string)
};

}
}

#endif /* _SIM_CONCENTRATOR_ */
