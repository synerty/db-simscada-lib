#ifndef _SIM_DIG_PT_
#define _SIM_DIG_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"

#include "../sim/SimTelePt.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DigPt;
  class DigDeviceType;
  class DigClass;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DigPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DigPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DigPt , db_simscada::sim::TelePt>
{
  public:
    DigPt();
    virtual ~DigPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_RELATED_PROPERTY(deviceClass, DeviceClass, std::string, db_simscada::sim::DigClass)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(defaultState, DefaultState, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(deviceType, DeviceType, std::string, db_simscada::sim::DigDeviceType)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dpAux1, DpAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dpAux2, DpAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dpAux3, DpAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dpAux4, DpAux4, std::string)
};

}
}

#endif /* _SIM_DIG_PT_ */
