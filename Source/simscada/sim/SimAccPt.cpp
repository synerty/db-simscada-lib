#include "SimAccPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AccPt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::AccPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::AccPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, rollOverValue, RollOverValue, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::AccPt, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, startValue, StartValue, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, acAux3, AcAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, acAux4, AcAux4, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, deltaVal, DeltaVal, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, freezable, Freezable, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, reportType, ReportType, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, acAux1, AcAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AccPt, acAux2, AcAux2, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(AccPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

AccPt::AccPt()
{
  mPropRollOverValue.setup(this);
  mPropAlias.setup(this);
  mPropStartValue.setup(this);
  mPropAcAux3.setup(this);
  mPropAcAux4.setup(this);
  mPropDeltaVal.setup(this);
  mPropFreezable.setup(this);
  mPropReportType.setup(this);
  mPropAcAux1.setup(this);
  mPropAcAux2.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

AccPt::~AccPt()
{
}
// ----------------------------------------------------------------------------

bool
AccPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimAccPt;
  sModelRemoveFunction = &ModelT::removeSimAccPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::AccPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Acc Pt");
  CfgT::setSqlOpts()->setTableName("tblAccPts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(RollOverValuePropT::setupStatic(
    "RollOverValue",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(StartValuePropT::setupStatic(
    "StartValue",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AcAux3PropT::setupStatic(
    "acAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AcAux4PropT::setupStatic(
    "acAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DeltaValPropT::setupStatic(
    "DeltaVal",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(FreezablePropT::setupStatic(
    "Freezable",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ReportTypePropT::setupStatic(
    "ReportType",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(AcAux1PropT::setupStatic(
    "acAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AcAux2PropT::setupStatic(
    "acAux_2",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
