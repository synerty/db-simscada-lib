#include "SimRtu.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Rtu, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Rtu, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Rtu>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::Rtu, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, addr1, Addr1, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, addr2, Addr2, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, delay, Delay, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, dialInMode, DialInMode, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, phoneNumber, PhoneNumber, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, protocolVariant, ProtocolVariant, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, rbemode, Rbemode, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, retries, Retries, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, startOnline, StartOnline, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, timeout, Timeout, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, rtAux1, RtAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, rtAux2, RtAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, rtAux3, RtAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, rtAux4, RtAux4, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, commissioningState, CommissioningState, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Rtu, initialTime, InitialTime, int32_t)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Rtu)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Rtu::Rtu()
{
  mPropAlias.setup(this);
  mPropAddr1.setup(this);
  mPropAddr2.setup(this);
  mPropDelay.setup(this);
  mPropDialInMode.setup(this);
  mPropPhoneNumber.setup(this);
  mPropProtocolVariant.setup(this);
  mPropRbemode.setup(this);
  mPropRetries.setup(this);
  mPropStartOnline.setup(this);
  mPropTimeout.setup(this);
  mPropRtAux1.setup(this);
  mPropRtAux2.setup(this);
  mPropRtAux3.setup(this);
  mPropRtAux4.setup(this);
  mPropCommissioningState.setup(this);
  mPropInitialTime.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

Rtu::~Rtu()
{
}
// ----------------------------------------------------------------------------

bool
Rtu::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimRtu;
  sModelRemoveFunction = &ModelT::removeSimRtu;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Rtu > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Rtu");
  CfgT::setSqlOpts()->setTableName("tblRtus");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(Addr1PropT::setupStatic(
    "Addr1",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(Addr2PropT::setupStatic(
    "Addr2",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(DelayPropT::setupStatic(
    "Delay",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(DialInModePropT::setupStatic(
    "DialInMode",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(PhoneNumberPropT::setupStatic(
    "PhoneNumber",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(ProtocolVariantPropT::setupStatic(
    "ProtocolVariant",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RbemodePropT::setupStatic(
    "RBEMode",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(RetriesPropT::setupStatic(
    "Retries",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(StartOnlinePropT::setupStatic(
    "StartOnline",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TimeoutPropT::setupStatic(
    "Timeout",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(RtAux1PropT::setupStatic(
    "rtAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RtAux2PropT::setupStatic(
    "rtAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RtAux3PropT::setupStatic(
    "rtAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(RtAux4PropT::setupStatic(
    "rtAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CommissioningStatePropT::setupStatic(
    "CommissioningState",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(InitialTimePropT::setupStatic(
    "InitialTime",
    false,
    true,
    boost::optional<int32_t>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
