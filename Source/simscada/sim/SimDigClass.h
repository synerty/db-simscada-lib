#ifndef _SIM_DIG_CLASS_
#define _SIM_DIG_CLASS_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DigClass;
  class DigPt;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DigClass, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DigClass
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DigClass >
{
  public:
    DigClass();
    virtual ~DigClass();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(stateBText, StateBText, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(stateAText, StateAText, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(description, Description, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(stateCText, StateCText, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(stateDText, StateDText, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(digPtDeviceClasss, DigPtDeviceClasss, db_simscada::sim::DigPt)
};

}
}

#endif /* _SIM_DIG_CLASS_ */
