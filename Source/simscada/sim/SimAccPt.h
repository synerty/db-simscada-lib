#ifndef _SIM_ACC_PT_
#define _SIM_ACC_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimTelePt.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class AccPt;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AccPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR AccPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::AccPt , db_simscada::sim::TelePt>
{
  public:
    AccPt();
    virtual ~AccPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(rollOverValue, RollOverValue, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(startValue, StartValue, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(acAux3, AcAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(acAux4, AcAux4, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(deltaVal, DeltaVal, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(freezable, Freezable, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(reportType, ReportType, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(acAux1, AcAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(acAux2, AcAux2, std::string)
};

}
}

#endif /* _SIM_ACC_PT_ */
