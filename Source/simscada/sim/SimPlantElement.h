#ifndef _SIM_PLANT_ELEMENT_
#define _SIM_PLANT_ELEMENT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class PlantElement;
  class Plant;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::PlantElement, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR PlantElement
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::PlantElement >
{
  public:
    PlantElement();
    virtual ~PlantElement();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(defSimNetRep, DefSimNetRep, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(plant, Plant, std::string, db_simscada::sim::Plant)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(id, Id, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(point, Point, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(roleDesc, RoleDesc, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(elementType, ElementType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(role, Role, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(value, Value, std::string)
};

}
}

#endif /* _SIM_PLANT_ELEMENT_ */
