#ifndef _SIM_ANA_PT_
#define _SIM_ANA_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"

#include "../sim/SimTelePt.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class AnaPt;
  class AnaProfile;
  class AnaDeviceType;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AnaPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR AnaPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::AnaPt , db_simscada::sim::TelePt>
{
  public:
    AnaPt();
    virtual ~AnaPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(defaultValue, DefaultValue, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(engMax, EngMax, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rawMin, RawMin, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(varyRange, VaryRange, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(engMin, EngMin, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rawMax, RawMax, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(anaDataType, AnaDataType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(alarmHi, AlarmHi, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(alarmHiHi, AlarmHiHi, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(alarmLo, AlarmLo, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(alarmLoLo, AlarmLoLo, double)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(processLimits, ProcessLimits, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(anAux1, AnAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(anAux2, AnAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(anAux3, AnAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(anAux4, AnAux4, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(deviceType, DeviceType, std::string, db_simscada::sim::AnaDeviceType)
    ORM_V1_DECLARE_RELATED_PROPERTY(profileName, ProfileName, std::string, db_simscada::sim::AnaProfile)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(jitter, Jitter, double)
};

}
}

#endif /* _SIM_ANA_PT_ */
