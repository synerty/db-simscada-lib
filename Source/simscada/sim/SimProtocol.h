#ifndef _SIM_PROTOCOL_
#define _SIM_PROTOCOL_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Protocol;
  class Object;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Protocol, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Protocol
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Protocol >
{
  public:
    Protocol();
    virtual ~Protocol();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(name, Name, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(objectProtocols, ObjectProtocols, db_simscada::sim::Object)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(timeStamp, TimeStamp, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(varyCount, VaryCount, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(varyTime, VaryTime, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(active, Active, std::string)
};

}
}

#endif /* _SIM_PROTOCOL_ */
