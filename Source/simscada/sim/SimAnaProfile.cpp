#include "SimAnaProfile.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AnaProfile, std::string, profileName) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaProfile, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::AnaProfile>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaProfile, profileName, ProfileName, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::AnaProfile, anaPtProfileNames, AnaPtProfileNames, db_simscada::sim::AnaPt)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::AnaProfile, fileName, FileName, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(AnaProfile)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

AnaProfile::AnaProfile()
{
  mPropProfileName.setup(this);
  mPropFileName.setup(this);

  // Related back references.
  mPropAnaPtProfileNames.setup(this);
}
// ----------------------------------------------------------------------------

AnaProfile::~AnaProfile()
{
}
// ----------------------------------------------------------------------------

bool
AnaProfile::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimAnaProfile;
  sModelRemoveFunction = &ModelT::removeSimAnaProfile;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::AnaProfile > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ana Profile");
  CfgT::setSqlOpts()->setTableName("tblAnaProfiles");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(ProfileNamePropT::setupStatic(
    "ProfileName",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(FileNamePropT::setupStatic(
    "FileName",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
