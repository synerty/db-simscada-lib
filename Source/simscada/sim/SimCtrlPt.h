#ifndef _SIM_CTRL_PT_
#define _SIM_CTRL_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedProperty.h"

#include "../sim/SimTelePt.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class CtrlPt;
  class CtrlDeviceType;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::CtrlPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR CtrlPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::CtrlPt , db_simscada::sim::TelePt>
{
  public:
    CtrlPt();
    virtual ~CtrlPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef1, CtrlDef1, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef2, CtrlDef2, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef3, CtrlDef3, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef4, CtrlDef4, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef5, CtrlDef5, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlDef6, CtrlDef6, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctrlType, CtrlType, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(opcctrlCode, OpcctrlCode, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(opcnumBits, OpcnumBits, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cpAux1, CpAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cpAux2, CpAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cpAux3, CpAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(cpAux4, CpAux4, std::string)
    ORM_V1_DECLARE_RELATED_PROPERTY(deviceType, DeviceType, std::string, db_simscada::sim::CtrlDeviceType)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
};

}
}

#endif /* _SIM_CTRL_PT_ */
