#include "SimCtrlPt.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"
#include "../sim/SimCtrlDeviceType.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::CtrlPt, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlPt, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlPt>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef1, CtrlDef1, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef2, CtrlDef2, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef3, CtrlDef3, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef4, CtrlDef4, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef5, CtrlDef5, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlDef6, CtrlDef6, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, ctrlType, CtrlType, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, opcctrlCode, OpcctrlCode, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, opcnumBits, OpcnumBits, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, cpAux1, CpAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, cpAux2, CpAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, cpAux3, CpAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlPt, cpAux4, CpAux4, std::string)
ORM_V1_DEFINE_RELATED_PROPERTY(db_simscada::SimScadaModel, db_simscada::sim::CtrlPt, deviceType, DeviceType, std::string, db_simscada::sim::CtrlDeviceType)
ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::CtrlPt, alias, Alias, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(CtrlPt)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

CtrlPt::CtrlPt()
{
  mPropCtrlDef1.setup(this);
  mPropCtrlDef2.setup(this);
  mPropCtrlDef3.setup(this);
  mPropCtrlDef4.setup(this);
  mPropCtrlDef5.setup(this);
  mPropCtrlDef6.setup(this);
  mPropCtrlType.setup(this);
  mPropOpcctrlCode.setup(this);
  mPropOpcnumBits.setup(this);
  mPropCpAux1.setup(this);
  mPropCpAux2.setup(this);
  mPropCpAux3.setup(this);
  mPropCpAux4.setup(this);
  mPropDeviceType.setup(this);
  mPropAlias.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

CtrlPt::~CtrlPt()
{
}
// ----------------------------------------------------------------------------

bool
CtrlPt::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimCtrlPt;
  sModelRemoveFunction = &ModelT::removeSimCtrlPt;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::CtrlPt > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ctrl Pt");
  CfgT::setSqlOpts()->setTableName("tblCtrlPts");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(CtrlDef1PropT::setupStatic(
    "CtrlDef1",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CtrlDef2PropT::setupStatic(
    "CtrlDef2",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CtrlDef3PropT::setupStatic(
    "CtrlDef3",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CtrlDef4PropT::setupStatic(
    "CtrlDef4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CtrlDef5PropT::setupStatic(
    "CtrlDef5",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CtrlDef6PropT::setupStatic(
    "CtrlDef6",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CtrlTypePropT::setupStatic(
    "CtrlType",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(OpcctrlCodePropT::setupStatic(
    "OPCCtrlCode",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(OpcnumBitsPropT::setupStatic(
    "OPCNumBits",
    false,
    true,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(CpAux1PropT::setupStatic(
    "cpAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CpAux2PropT::setupStatic(
    "cpAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CpAux3PropT::setupStatic(
    "cpAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(CpAux4PropT::setupStatic(
    "cpAux_4",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DeviceTypePropT::setupStatic(
    "DeviceType",
    &db_simscada::sim::CtrlDeviceType::id,
    &SimScadaModel::SimCtrlDeviceType,
    &db_simscada::sim::CtrlDeviceType::CtrlPtDeviceTypes,
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
