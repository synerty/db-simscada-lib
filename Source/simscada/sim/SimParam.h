#ifndef _SIM_PARAM_
#define _SIM_PARAM_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class Param;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Param, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR Param
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::Param >
{
  public:
    Param();
    virtual ~Param();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(paramName, ParamName, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(paramValue, ParamValue, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(purpose, Purpose, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(units, Units, std::string)
};

}
}

#endif /* _SIM_PARAM_ */
