#include "SimCtrlDeviceType.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::CtrlDeviceType, std::string, id) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlDeviceType, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::CtrlDeviceType>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlDeviceType, description, Description, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::CtrlDeviceType, id, Id, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::CtrlDeviceType, ctrlPtDeviceTypes, CtrlPtDeviceTypes, db_simscada::sim::CtrlPt)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(CtrlDeviceType)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

CtrlDeviceType::CtrlDeviceType()
{
  mPropDescription.setup(this);
  mPropId.setup(this);

  // Related back references.
  mPropCtrlPtDeviceTypes.setup(this);
}
// ----------------------------------------------------------------------------

CtrlDeviceType::~CtrlDeviceType()
{
}
// ----------------------------------------------------------------------------

bool
CtrlDeviceType::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimCtrlDeviceType;
  sModelRemoveFunction = &ModelT::removeSimCtrlDeviceType;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::CtrlDeviceType > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Ctrl Device Type");
  CfgT::setSqlOpts()->setTableName("tblCtrlDeviceTypes");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(DescriptionPropT::setupStatic(
    "Description",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(IdPropT::setupStatic(
    "ID",
    true,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
