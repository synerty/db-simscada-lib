#ifndef _SIM_STR_PT_
#define _SIM_STR_PT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class StrPt;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::StrPt, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR StrPt
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::StrPt , db_simscada::sim::Object>
{
  public:
    StrPt();
    virtual ~StrPt();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(defaultString, DefaultString, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(spAux1, SpAux1, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(spAux2, SpAux2, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(spAux3, SpAux3, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(spAux4, SpAux4, std::string)
};

}
}

#endif /* _SIM_STR_PT_ */
