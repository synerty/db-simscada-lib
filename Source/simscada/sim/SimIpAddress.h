#ifndef _SIM_IP_ADDRESS_
#define _SIM_IP_ADDRESS_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class IpAddress;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::IpAddress, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR IpAddress
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::IpAddress >
{
  public:
    IpAddress();
    virtual ~IpAddress();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(subnetMask, SubnetMask, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ipaddr, Ipaddr, std::string)
};

}
}

#endif /* _SIM_IP_ADDRESS_ */
