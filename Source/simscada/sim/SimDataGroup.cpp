#include "SimDataGroup.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"
#include "orm/v1/SqlOpts.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DataGroup, std::string, alias) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataGroup, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::DataGroup>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY_INHERITED(db_simscada::sim::DataGroup, alias, Alias, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, type, Type, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, address, Address, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, dgAux1, DgAux1, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, dgAux2, DgAux2, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, dgAux3, DgAux3, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::DataGroup, dgAux4, DgAux4, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(DataGroup)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

DataGroup::DataGroup()
{
  mPropAlias.setup(this);
  mPropType.setup(this);
  mPropAddress.setup(this);
  mPropDgAux1.setup(this);
  mPropDgAux2.setup(this);
  mPropDgAux3.setup(this);
  mPropDgAux4.setup(this);

  // Related back references.
}
// ----------------------------------------------------------------------------

DataGroup::~DataGroup()
{
}
// ----------------------------------------------------------------------------

bool
DataGroup::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimDataGroup;
  sModelRemoveFunction = &ModelT::removeSimDataGroup;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::DataGroup > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Data Group");
  CfgT::setSqlOpts()->setTableName("tblDataGroups");

  CfgT::setSqlOpts()->setInheritanceType(orm::v1::SqlOpts::JoinedInheritance);

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(AliasPropT::setupStatic(
    "Alias",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TypePropT::setupStatic(
    "Type",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(AddressPropT::setupStatic(
    "Address",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DgAux1PropT::setupStatic(
    "dgAux_1",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DgAux2PropT::setupStatic(
    "dgAux_2",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DgAux3PropT::setupStatic(
    "dgAux_3",
    false,
    true,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(DgAux4PropT::setupStatic(
    "dgAux_4",
    false,
    true,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
