#ifndef _SIM_DATA_PORT_
#define _SIM_DATA_PORT_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"

#include "../sim/SimObject.h"

namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class DataPort;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::DataPort, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR DataPort
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::DataPort , db_simscada::sim::Object>
{
  public:
    DataPort();
    virtual ~DataPort();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY_INHERITED(alias, Alias, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(portNum, PortNum, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(type, Type, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(active, Active, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(baud, Baud, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ctsflow, Ctsflow, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(coalesceMsgs, CoalesceMsgs, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dsrflow, Dsrflow, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dtrctrl, Dtrctrl, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(dataBits, DataBits, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(expectReflection, ExpectReflection, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(halfDuplex, HalfDuplex, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ipaddr, Ipaddr, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(parity, Parity, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(postTxRtstime, PostTxRtstime, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(preTxRtstime, PreTxRtstime, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(ptmBytes, PtmBytes, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(rtsctrl, Rtsctrl, std::string)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(squelch, Squelch, int32_t)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(stopBits, StopBits, int32_t)
};

}
}

#endif /* _SIM_DATA_PORT_ */
