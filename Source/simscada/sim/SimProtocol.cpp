#include "SimProtocol.h"

#include <orm/v1/StorableWrapper.ini>
#include <orm/v1/StorageWrapper.ini>

#include "../SimScadaModel.h"


ORM_V1_DEFINE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::Protocol, std::string, name) 
template class orm::v1::StorageWrapper<db_simscada::SimScadaModel, db_simscada::sim::Protocol, std::string>;
template class orm::v1::StorableWrapper<db_simscada::SimScadaModel, db_simscada::sim::Protocol>;
// ----------------------------------------------------------------------------

ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Protocol, name, Name, std::string)
ORM_V1_DEFINE_RELATED_TO_MANY_PROPERTY(db_simscada::sim::Protocol, objectProtocols, ObjectProtocols, db_simscada::sim::Object)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Protocol, timeStamp, TimeStamp, std::string)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Protocol, varyCount, VaryCount, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Protocol, varyTime, VaryTime, int32_t)
ORM_V1_DEFINE_SIMPLE_PROPERTY(db_simscada::sim::Protocol, active, Active, std::string)
// ----------------------------------------------------------------------------

namespace db_simscada {
namespace sim {

ORM_V1_CALL_STORABLE_SETUP(Protocol)
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

Protocol::Protocol()
{
  mPropName.setup(this);
  mPropTimeStamp.setup(this);
  mPropVaryCount.setup(this);
  mPropVaryTime.setup(this);
  mPropActive.setup(this);

  // Related back references.
  mPropObjectProtocols.setup(this);
}
// ----------------------------------------------------------------------------

Protocol::~Protocol()
{
}
// ----------------------------------------------------------------------------

bool
Protocol::setupStatic()
{

  sModelAddDerrivedFunction = &ModelT::addDerrivedSimProtocol;
  sModelRemoveFunction = &ModelT::removeSimProtocol;

  using namespace orm::v1;
  typedef StorableConfig< db_simscada::sim::Protocol > CfgT;

  CfgT::setSqlOpts()->setName("Sim - Protocol");
  CfgT::setSqlOpts()->setTableName("tblProtocols");

  CfgT::setSqlOpts()->addCondition("1 = 1 OR 1 = 0");

  CfgT::addFieldMeta(NamePropT::setupStatic(
    "Name",
    true,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(TimeStampPropT::setupStatic(
    "TimeStamp",
    false,
    false,
    boost::optional<std::string>()));
  CfgT::addFieldMeta(VaryCountPropT::setupStatic(
    "VaryCount",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(VaryTimePropT::setupStatic(
    "VaryTime",
    false,
    false,
    boost::optional<int32_t>()));
  CfgT::addFieldMeta(ActivePropT::setupStatic(
    "Active",
    false,
    false,
    boost::optional<std::string>()));

  return true;
}
// ----------------------------------------------------------------------------


}
}
