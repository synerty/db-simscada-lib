#ifndef _SIM_ANA_PROFILE_
#define _SIM_ANA_PROFILE_

#if defined(BUILD_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllexport)
#elif defined(USE_DB_SIMSCADA_DLL)
#define DECL_DB_SIMSCADA_DIR __declspec(dllimport)
#else
#define DECL_DB_SIMSCADA_DIR 
#endif

#include <string>
#include <stdint.h>

#include "orm/v1/Storable.h"
#include "orm/v1/StorableWrapper.h"
#include "orm/v1/SimpleProperty.h"
#include "orm/v1/RelatedToManyProperty.h"


namespace db_simscada {
  class SimScadaModel;

namespace sim {
  class AnaProfile;
  class AnaPt;
}

}

ORM_V1_DECLARE_SPECIALISED_STORABLE_KEY_WRAPPER(db_simscada::sim::AnaProfile, std::string)

namespace db_simscada {
namespace sim {


class DECL_DB_SIMSCADA_DIR AnaProfile
    : public orm::v1::Storable< db_simscada::SimScadaModel, db_simscada::sim::AnaProfile >
{
  public:
    AnaProfile();
    virtual ~AnaProfile();

  public:
    static bool setupStatic();

  public:

    ORM_V1_DECLARE_SIMPLE_PROPERTY(profileName, ProfileName, std::string)
    ORM_V1_DECLARE_RELATED_TO_MANY_PROPERTY(anaPtProfileNames, AnaPtProfileNames, db_simscada::sim::AnaPt)
    ORM_V1_DECLARE_SIMPLE_PROPERTY(fileName, FileName, std::string)
};

}
}

#endif /* _SIM_ANA_PROFILE_ */
