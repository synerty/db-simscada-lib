--
-- PostgreSQL database dump
--

-- Dumped from database version 9.0.4
-- Dumped by pg_dump version 9.0.4
-- Started on 2015-07-05 16:27:06

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 7 (class 2615 OID 55656)
-- Name: sams_meta; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sams_meta;


ALTER SCHEMA sams_meta OWNER TO postgres;

SET search_path = sams_meta, pg_catalog;

--
-- TOC entry 1517 (class 1259 OID 55657)
-- Dependencies: 7
-- Name: seq_ID; Type: SEQUENCE; Schema: sams_meta; Owner: postgres
--

CREATE SEQUENCE "seq_ID"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sams_meta."seq_ID" OWNER TO postgres;

--
-- TOC entry 1848 (class 0 OID 0)
-- Dependencies: 1517
-- Name: seq_ID; Type: SEQUENCE SET; Schema: sams_meta; Owner: postgres
--

SELECT pg_catalog.setval('"seq_ID"', 271, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 1518 (class 1259 OID 55659)
-- Dependencies: 1801 1802 1803 1804 1805 7
-- Name: tbl_COLUMN; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_COLUMN" (
    "ID" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "TABLE" integer NOT NULL,
    "VALUE_TYPE" integer,
    "DATA_TYPE" character varying(6) DEFAULT 'int4'::character varying NOT NULL,
    "DATA_LENGTH" integer,
    "IS_NULLABLE" boolean DEFAULT false NOT NULL,
    "RELATES_TO" integer,
    "DEFAULT_VALUE" character varying,
    "MANUALLY_MODIFIED" boolean DEFAULT false NOT NULL,
    "GENERATE_STORABLE_PROPERTY" boolean DEFAULT true NOT NULL,
    "PRIMARY_KEY" boolean DEFAULT false NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_COLUMN" OWNER TO postgres;

--
-- TOC entry 1519 (class 1259 OID 55670)
-- Dependencies: 1806 7
-- Name: tbl_OBJECT_TYPE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_OBJECT_TYPE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "NAME" character varying,
    "INHERITS" integer,
    "DATA_TABLE" integer,
    "OBJECT_TYPE_TABLE" integer
);


ALTER TABLE sams_meta."tbl_OBJECT_TYPE" OWNER TO postgres;

--
-- TOC entry 1849 (class 0 OID 0)
-- Dependencies: 1519
-- Name: TABLE "tbl_OBJECT_TYPE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON TABLE "tbl_OBJECT_TYPE" IS 'Describes the different types of objects.';


--
-- TOC entry 1850 (class 0 OID 0)
-- Dependencies: 1519
-- Name: COLUMN "tbl_OBJECT_TYPE"."INHERITS"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_OBJECT_TYPE"."INHERITS" IS 'The object type this object type inherits from.
IE :
Channel
-> DNP Channel';


--
-- TOC entry 1851 (class 0 OID 0)
-- Dependencies: 1519
-- Name: COLUMN "tbl_OBJECT_TYPE"."DATA_TABLE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_OBJECT_TYPE"."DATA_TABLE" IS 'The table containing the data for this object variant.The table must be the last table in the joined table inheritance, IE the least common table.';


--
-- TOC entry 1520 (class 1259 OID 55677)
-- Dependencies: 7
-- Name: tbl_SCHEMA; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_SCHEMA" (
    "ID" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_SCHEMA" OWNER TO postgres;

--
-- TOC entry 1852 (class 0 OID 0)
-- Dependencies: 1520
-- Name: TABLE "tbl_SCHEMA"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON TABLE "tbl_SCHEMA" IS 'Describes the schemas in this database';


--
-- TOC entry 1521 (class 1259 OID 55683)
-- Dependencies: 1807 1808 7
-- Name: tbl_TABLE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_TABLE" (
    "ID" integer NOT NULL,
    "INHERITS" integer,
    "SCHEMA" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "INHERIT_TYPE" character varying(8),
    "GENERATE_STORABLE" boolean DEFAULT true NOT NULL,
    "INHERITS_IS_USER_DEFINED" boolean DEFAULT false NOT NULL,
    "NICE_NAME" character varying
);


ALTER TABLE sams_meta."tbl_TABLE" OWNER TO postgres;

--
-- TOC entry 1522 (class 1259 OID 55691)
-- Dependencies: 1809 7
-- Name: tbl_VALUE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_VALUE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "VALUE_TYPE" integer NOT NULL,
    "NAME" character varying NOT NULL,
    "TOOLTIP" character varying,
    "HELP" character varying
);


ALTER TABLE sams_meta."tbl_VALUE" OWNER TO postgres;

--
-- TOC entry 1853 (class 0 OID 0)
-- Dependencies: 1522
-- Name: COLUMN "tbl_VALUE"."TOOLTIP"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE"."TOOLTIP" IS 'Enter the tooltop that will be displayed when this option is hovered over in the GUI.';


--
-- TOC entry 1854 (class 0 OID 0)
-- Dependencies: 1522
-- Name: COLUMN "tbl_VALUE"."HELP"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE"."HELP" IS 'Add application help for this value here.';


--
-- TOC entry 1523 (class 1259 OID 55698)
-- Dependencies: 1810 1811 7
-- Name: tbl_VALUE_TYPE; Type: TABLE; Schema: sams_meta; Owner: postgres; Tablespace: 
--

CREATE TABLE "tbl_VALUE_TYPE" (
    "ID" integer DEFAULT nextval('"seq_ID"'::regclass) NOT NULL,
    "NAME" character varying NOT NULL,
    "COMMENT" character varying,
    "GENERATE_ENUM" boolean DEFAULT true NOT NULL,
    "TABLE" integer
);


ALTER TABLE sams_meta."tbl_VALUE_TYPE" OWNER TO postgres;

--
-- TOC entry 1855 (class 0 OID 0)
-- Dependencies: 1523
-- Name: COLUMN "tbl_VALUE_TYPE"."TABLE"; Type: COMMENT; Schema: sams_meta; Owner: postgres
--

COMMENT ON COLUMN "tbl_VALUE_TYPE"."TABLE" IS 'The table containing the data for this value type.The table must be the last table in the joined table inheritance, IE the least common table.';


--
-- TOC entry 1840 (class 0 OID 55659)
-- Dependencies: 1518
-- Data for Name: tbl_COLUMN; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_COLUMN" ("ID", "NAME", "TABLE", "VALUE_TYPE", "DATA_TYPE", "DATA_LENGTH", "IS_NULLABLE", "RELATES_TO", "DEFAULT_VALUE", "MANUALLY_MODIFIED", "GENERATE_STORABLE_PROPERTY", "PRIMARY_KEY", "NICE_NAME") FROM stdin;
33	Alias	2	\N	string	0	f	\N	\N	f	t	t	\N
37	RollOverValue	2	\N	int4	0	f	\N	\N	f	t	f	\N
38	StartValue	2	\N	int4	0	f	\N	\N	f	t	f	\N
45	FileName	4	\N	string	0	f	\N	\N	f	t	f	\N
46	ProfileName	4	\N	string	0	f	\N	\N	f	t	t	\N
51	Alias	5	\N	string	0	f	\N	\N	f	t	t	\N
53	DefaultValue	5	\N	float8	0	f	\N	\N	f	t	f	\N
55	EngMax	5	\N	float8	0	f	\N	\N	f	t	f	\N
56	EngMin	5	\N	float8	0	f	\N	\N	f	t	f	\N
62	RawMax	5	\N	int4	0	f	\N	\N	f	t	f	\N
63	RawMin	5	\N	int4	0	f	\N	\N	f	t	f	\N
64	VaryRange	5	\N	float8	0	f	\N	\N	f	t	f	\N
69	Alias	6	\N	string	0	f	\N	\N	f	t	t	\N
92	Delay	9	\N	int4	0	f	\N	\N	t	t	f	\N
73	MsgSource	6	\N	string	0	f	\N	\N	f	t	f	\N
74	POA	6	\N	string	0	f	\N	\N	f	t	f	\N
94	ScanPoint	9	\N	string	0	f	249	\N	t	t	f	\N
82	Alias	7	\N	string	0	f	\N	\N	f	t	t	\N
95	StateTo	9	\N	string	0	f	\N	\N	f	t	f	\N
90	ID	8	\N	string	0	f	\N	\N	f	t	t	\N
93	Priority	9	\N	int4	0	f	\N	\N	f	t	f	\N
111	Alias	11	\N	string	0	f	\N	\N	f	t	t	\N
112	BlockNumber	11	\N	int4	0	f	\N	\N	f	t	f	\N
113	ChannelAlias	11	\N	string	0	f	\N	\N	f	t	f	\N
117	Type	11	\N	string	0	f	\N	\N	f	t	f	\N
120	Alias	12	\N	string	0	f	\N	\N	f	t	t	\N
121	Type	12	\N	string	0	f	\N	\N	f	t	f	\N
127	Alias	13	\N	string	0	f	\N	\N	f	t	t	\N
138	PortNum	13	\N	int4	0	f	\N	\N	f	t	f	\N
145	Type	13	\N	string	0	f	\N	\N	f	t	f	\N
147	ID	14	\N	string	0	f	\N	\N	f	t	t	\N
153	DoubleBit	15	\N	string	0	f	\N	\N	f	t	f	\N
154	ID	15	\N	string	0	f	\N	\N	f	t	t	\N
155	StateA	15	\N	int4	0	f	\N	\N	f	t	f	\N
156	StateB	15	\N	int4	0	f	\N	\N	f	t	f	\N
229	Protocol	27	\N	string	0	f	198	\N	t	t	f	\N
159	Alias	16	\N	string	0	f	\N	\N	f	t	t	\N
160	DefaultState	16	\N	string	0	f	\N	\N	f	t	f	\N
169	IPAddr	18	\N	string	0	f	\N	\N	f	t	t	\N
170	SubnetMask	18	\N	string	0	f	\N	\N	f	t	f	\N
174	AttributeName	20	\N	string	0	f	\N	\N	f	t	t	\N
176	ModemTypeID	20	\N	string	0	f	\N	\N	f	t	t	\N
177	Alias	21	\N	string	0	f	\N	\N	f	t	t	\N
179	Alias	22	\N	string	0	f	\N	\N	f	t	t	\N
181	TypeID	22	\N	string	0	f	\N	\N	f	t	f	\N
182	Alias	23	\N	string	0	f	\N	\N	f	t	t	\N
184	Name	23	\N	string	0	f	\N	\N	f	t	f	\N
186	PlantType	23	\N	string	0	f	\N	\N	f	t	f	\N
187	Alias	24	\N	string	0	f	\N	\N	f	t	t	\N
188	DefSimNetRep	24	\N	string	0	f	\N	\N	f	t	f	\N
189	ElementType	24	\N	string	0	f	\N	\N	f	t	f	\N
191	Name	24	\N	string	0	f	\N	\N	f	t	f	\N
194	Role	24	\N	int4	0	f	\N	\N	f	t	f	\N
197	Active	25	\N	string	0	f	\N	\N	f	t	f	\N
198	Name	25	\N	string	0	f	\N	\N	f	t	t	\N
199	TimeStamp	25	\N	string	0	f	\N	\N	f	t	f	\N
200	VaryCount	25	\N	int4	0	f	\N	\N	f	t	f	\N
201	VaryTime	25	\N	int4	0	f	\N	\N	f	t	f	\N
204	Alias	26	\N	string	0	f	\N	\N	f	t	t	\N
227	ObjectType	27	\N	string	0	f	\N	\N	f	t	f	\N
149	StateBText	14	\N	string	0	f	\N	\N	f	t	f	state b text
148	StateAText	14	\N	string	0	f	\N	\N	f	t	f	state a text
172	LinkedAlias	19	\N	string	0	f	249	\N	t	t	f	\N
218	Alias	27	\N	string	0	f	\N	\N	f	t	t	\N
171	LinkType	19	\N	string	0	f	\N	\N	t	t	f	\N
226	Name	27	\N	string	0	f	\N	\N	f	t	f	\N
173	PointAlias	19	\N	string	0	f	\N	\N	f	t	t	\N
233	ParamName	28	\N	string	0	f	\N	\N	f	t	t	\N
234	ParamValue	28	\N	string	0	f	\N	\N	f	t	f	\N
237	Alias	29	\N	string	0	f	\N	\N	f	t	t	\N
238	DefaultString	29	\N	string	0	f	\N	\N	f	t	f	\N
249	Alias	30	\N	string	0	f	\N	\N	f	t	t	\N
161	DeviceClass	16	\N	string	0	f	147	\N	t	t	f	\N
228	Parent	27	\N	string	0	t	218	\N	t	t	f	\N
44	ID	3	\N	string	0	f	\N	\N	f	t	t	\N
222	IntPassedNum	27	\N	int4	0	t	\N	\N	f	t	f	\N
192	Plant	24	\N	string	0	f	182	\N	t	t	f	\N
221	IntFailedNum	27	\N	int4	0	t	\N	\N	f	t	f	\N
52	AnaDataType	5	\N	string	0	f	\N	\N	t	t	f	\N
162	DeviceType	16	\N	string	0	f	154	\N	t	t	f	\N
219	ErrorDesc	27	\N	string	0	t	\N	\N	f	t	f	\N
220	ExternalID	27	\N	string	0	t	\N	\N	f	t	f	\N
89	Description	8	\N	string	0	t	\N	\N	f	t	f	\N
146	Description	14	\N	string	0	t	\N	\N	f	t	f	\N
150	StateCText	14	\N	string	0	t	\N	\N	f	t	f	state c text
151	StateDText	14	\N	string	0	t	\N	\N	f	t	f	state d text
152	Description	15	\N	string	0	t	\N	\N	f	t	f	\N
157	StateC	15	\N	int4	0	t	\N	\N	f	t	f	\N
158	StateD	15	\N	int4	0	t	\N	\N	f	t	f	\N
175	AttributeValue	20	\N	string	0	t	\N	\N	f	t	f	\N
178	ModemSelectionScheme	21	\N	string	0	t	\N	\N	f	t	f	\N
180	ModemPoolAlias	22	\N	string	0	t	\N	\N	f	t	f	\N
183	ID	23	\N	string	0	t	\N	\N	f	t	f	\N
185	Parent	23	\N	string	0	t	\N	\N	f	t	f	\N
190	ID	24	\N	string	0	t	\N	\N	f	t	f	\N
193	Point	24	\N	string	0	t	\N	\N	f	t	f	\N
195	RoleDesc	24	\N	string	0	t	\N	\N	f	t	f	\N
196	Value	24	\N	string	0	t	\N	\N	f	t	f	\N
232	SourceRefVer	27	\N	string	0	t	\N	\N	f	t	f	\N
225	Invisible	27	\N	string	0	t	\N	\N	f	t	f	\N
231	SourceRefDate	27	\N	float8	0	t	\N	\N	f	t	f	\N
230	SourceRef	27	\N	string	0	t	\N	\N	f	t	f	\N
224	IntegrityDate	27	\N	float8	0	t	\N	\N	f	t	f	\N
223	IntResState	27	\N	int4	0	t	\N	\N	f	t	f	\N
235	Purpose	28	\N	string	0	t	\N	\N	f	t	f	\N
236	Units	28	\N	string	0	t	\N	\N	f	t	f	\N
257	Alias	31	\N	string	0	t	\N	\N	f	t	f	\N
258	Comments	31	\N	string	0	t	\N	\N	f	t	f	\N
259	DateAndTime	31	\N	string	0	t	\N	\N	f	t	f	\N
260	Result	31	\N	string	0	t	\N	\N	f	t	f	\N
261	TestID	31	\N	int4	0	t	\N	\N	f	t	f	\N
262	ObjectType	32	\N	string	0	t	\N	\N	f	t	f	\N
263	TestDesc	32	\N	string	0	t	\N	\N	f	t	f	\N
264	TestID	32	\N	int4	0	t	\N	\N	f	t	f	\N
265	TestName	32	\N	string	0	t	\N	\N	f	t	f	\N
43	Description	3	\N	string	0	t	\N	\N	f	t	f	\N
34	DeltaVal	2	\N	int4	0	t	\N	\N	f	t	f	\N
35	Freezable	2	\N	string	0	t	\N	\N	f	t	f	\N
36	ReportType	2	\N	int4	0	t	\N	\N	f	t	f	\N
39	acAux_1	2	\N	string	0	t	\N	\N	f	t	f	\N
40	acAux_2	2	\N	string	0	t	\N	\N	f	t	f	\N
59	ProfileMax	5	\N	float8	0	t	\N	\N	f	f	f	\N
60	ProfileMin	5	\N	float8	0	t	\N	\N	f	f	f	\N
41	acAux_3	2	\N	string	0	t	\N	\N	f	t	f	\N
42	acAux_4	2	\N	string	0	t	\N	\N	f	t	f	\N
47	AlarmHi	5	\N	float8	0	t	\N	\N	f	t	f	\N
48	AlarmHiHi	5	\N	float8	0	t	\N	\N	f	t	f	\N
49	AlarmLo	5	\N	float8	0	t	\N	\N	f	t	f	\N
50	AlarmLoLo	5	\N	float8	0	t	\N	\N	f	t	f	\N
57	Jitter	5	\N	float8	0	t	\N	\N	f	t	f	\N
58	ProcessLimits	5	\N	string	0	t	\N	\N	f	t	f	\N
65	anAux_1	5	\N	string	0	t	\N	\N	f	t	f	\N
66	anAux_2	5	\N	string	0	t	\N	\N	f	t	f	\N
67	anAux_3	5	\N	string	0	t	\N	\N	f	t	f	\N
68	anAux_4	5	\N	string	0	t	\N	\N	f	t	f	\N
54	DeviceType	5	\N	string	0	t	44	\N	t	t	f	\N
61	ProfileName	5	\N	string	0	t	46	\N	t	t	f	\N
243	Addr_1	30	\N	int4	0	t	\N	\N	f	t	f	\N
244	Addr_2	30	\N	int4	0	t	\N	\N	f	t	f	\N
245	Addr_3	30	\N	int4	0	t	\N	\N	f	t	f	\N
246	Addr_4	30	\N	int4	0	t	\N	\N	f	t	f	\N
247	Addr_5	30	\N	string	0	t	\N	\N	f	t	f	\N
248	Addr_6	30	\N	string	0	t	\N	\N	f	t	f	\N
250	OPCOffset	30	\N	int4	0	t	\N	\N	f	t	f	\N
251	OPCTag	30	\N	string	0	t	\N	\N	f	t	f	\N
252	OPCVariantType	30	\N	string	0	t	\N	\N	f	t	f	\N
253	tpAux_1	30	\N	string	0	t	\N	\N	f	t	f	\N
254	tpAux_2	30	\N	string	0	t	\N	\N	f	t	f	\N
255	tpAux_3	30	\N	string	0	t	\N	\N	f	t	f	\N
256	tpAux_4	30	\N	string	0	t	\N	\N	f	t	f	\N
239	spAux_1	29	\N	string	0	t	\N	\N	f	t	f	\N
240	spAux_2	29	\N	string	0	t	\N	\N	f	t	f	\N
241	spAux_3	29	\N	string	0	t	\N	\N	f	t	f	\N
242	spAux_4	29	\N	string	0	t	\N	\N	f	t	f	\N
202	Addr1	26	\N	int4	0	t	\N	\N	f	t	f	\N
203	Addr2	26	\N	int4	0	t	\N	\N	f	t	f	\N
205	Delay	26	\N	int4	0	t	\N	\N	f	t	f	\N
206	DialInMode	26	\N	string	0	t	\N	\N	f	t	f	\N
208	PhoneNumber	26	\N	string	0	t	\N	\N	f	t	f	\N
209	ProtocolVariant	26	\N	string	0	t	\N	\N	f	t	f	\N
210	RBEMode	26	\N	int4	0	t	\N	\N	f	t	f	\N
211	Retries	26	\N	int4	0	t	\N	\N	f	t	f	\N
212	StartOnline	26	\N	string	0	t	\N	\N	f	t	f	\N
213	Timeout	26	\N	int4	0	t	\N	\N	f	t	f	\N
214	rtAux_1	26	\N	string	0	t	\N	\N	f	t	f	\N
215	rtAux_2	26	\N	string	0	t	\N	\N	f	t	f	\N
216	rtAux_3	26	\N	string	0	t	\N	\N	f	t	f	\N
217	rtAux_4	26	\N	string	0	t	\N	\N	f	t	f	\N
163	dpAux_1	16	\N	string	0	t	\N	\N	f	t	f	\N
164	dpAux_2	16	\N	string	0	t	\N	\N	f	t	f	\N
165	dpAux_3	16	\N	string	0	t	\N	\N	f	t	f	\N
166	dpAux_4	16	\N	string	0	t	\N	\N	f	t	f	\N
126	Active	13	\N	string	0	t	\N	\N	f	t	f	\N
128	Baud	13	\N	int4	0	t	\N	\N	f	t	f	\N
129	CTSFlow	13	\N	string	0	t	\N	\N	f	t	f	\N
130	CoalesceMsgs	13	\N	string	0	t	\N	\N	f	t	f	\N
131	DSRFlow	13	\N	string	0	t	\N	\N	f	t	f	\N
132	DTRCtrl	13	\N	string	0	t	\N	\N	f	t	f	\N
133	DataBits	13	\N	int4	0	t	\N	\N	f	t	f	\N
134	ExpectReflection	13	\N	string	0	t	\N	\N	f	t	f	\N
135	HalfDuplex	13	\N	string	0	t	\N	\N	f	t	f	\N
136	IPAddr	13	\N	string	0	t	\N	\N	f	t	f	\N
137	Parity	13	\N	string	0	t	\N	\N	f	t	f	\N
139	PostTxRTSTime	13	\N	int4	0	t	\N	\N	f	t	f	\N
140	PreTxRTSTime	13	\N	int4	0	t	\N	\N	f	t	f	\N
141	PtmBytes	13	\N	int4	0	t	\N	\N	f	t	f	\N
142	RTSCtrl	13	\N	string	0	t	\N	\N	f	t	f	\N
143	Squelch	13	\N	int4	0	t	\N	\N	f	t	f	\N
144	StopBits	13	\N	int4	0	t	\N	\N	f	t	f	\N
119	Address	12	\N	string	0	t	\N	\N	f	t	f	\N
122	dgAux_1	12	\N	string	0	t	\N	\N	f	t	f	\N
123	dgAux_2	12	\N	string	0	t	\N	\N	f	t	f	\N
124	dgAux_3	12	\N	string	0	t	\N	\N	f	t	f	\N
125	dgAux_4	12	\N	string	0	t	\N	\N	f	t	f	\N
114	CyclePeriod	11	\N	int4	0	t	\N	\N	f	t	f	\N
115	CycleSend	11	\N	string	0	t	\N	\N	f	t	f	\N
116	SpontSend	11	\N	string	0	t	\N	\N	f	t	f	\N
118	WordCount	11	\N	int4	0	t	\N	\N	f	t	f	\N
97	CtrlDef1	10	\N	int4	0	t	\N	\N	f	t	f	\N
98	CtrlDef2	10	\N	int4	0	t	\N	\N	f	t	f	\N
99	CtrlDef3	10	\N	int4	0	t	\N	\N	f	t	f	\N
100	CtrlDef4	10	\N	string	0	t	\N	\N	f	t	f	\N
101	CtrlDef5	10	\N	string	0	t	\N	\N	f	t	f	\N
102	CtrlDef6	10	\N	string	0	t	\N	\N	f	t	f	\N
103	CtrlType	10	\N	string	0	t	\N	\N	f	t	f	\N
105	OPCCtrlCode	10	\N	int4	0	t	\N	\N	f	t	f	\N
106	OPCNumBits	10	\N	int4	0	t	\N	\N	f	t	f	\N
107	cpAux_1	10	\N	string	0	t	\N	\N	f	t	f	\N
108	cpAux_2	10	\N	string	0	t	\N	\N	f	t	f	\N
109	cpAux_3	10	\N	string	0	t	\N	\N	f	t	f	\N
110	cpAux_4	10	\N	string	0	t	\N	\N	f	t	f	\N
104	DeviceType	10	\N	string	0	t	90	\N	t	t	f	\N
81	Address	7	\N	int4	0	t	\N	\N	f	t	f	\N
83	Password	7	\N	string	0	t	\N	\N	f	t	f	\N
84	UserName	7	\N	string	0	t	\N	\N	f	t	f	\N
85	coAux_1	7	\N	string	0	t	\N	\N	f	t	f	\N
86	coAux_2	7	\N	string	0	t	\N	\N	f	t	f	\N
87	coAux_3	7	\N	string	0	t	\N	\N	f	t	f	\N
88	coAux_4	7	\N	string	0	t	\N	\N	f	t	f	\N
70	ChannelNumber	6	\N	int4	0	t	\N	\N	f	t	f	\N
71	MasterIP	6	\N	string	0	t	\N	\N	f	t	f	\N
72	MasterPort	6	\N	int4	0	t	\N	\N	f	t	f	\N
75	PhoneNumber	6	\N	string	0	t	\N	\N	f	t	f	\N
76	ProtocolVariant	6	\N	string	0	t	\N	\N	f	t	f	\N
77	chAux_1	6	\N	string	0	t	\N	\N	f	t	f	\N
78	chAux_2	6	\N	string	0	t	\N	\N	f	t	f	\N
79	chAux_3	6	\N	string	0	t	\N	\N	f	t	f	\N
80	chAux_4	6	\N	string	0	t	\N	\N	f	t	f	\N
267	CommissioningState	30	\N	int4	0	t	\N	\N	f	t	f	\N
268	CommissioningState	26	\N	int4	0	t	\N	\N	f	t	f	\N
269	Alias	266	\N	string	0	f	\N	\N	f	t	f	\N
91	CtrlPoint	9	\N	string	0	f	\N	\N	t	t	t	\N
96	Alias	10	\N	string	0	f	\N	\N	t	t	t	\N
270	DragDropText	266	\N	string	0	f	\N	\N	f	t	t	drag drop text
271	InitialTime	26	\N	int4	0	t	\N	\N	f	t	f	\N
\.


--
-- TOC entry 1841 (class 0 OID 55670)
-- Dependencies: 1519
-- Data for Name: tbl_OBJECT_TYPE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_OBJECT_TYPE" ("ID", "NAME", "INHERITS", "DATA_TABLE", "OBJECT_TYPE_TABLE") FROM stdin;
\.


--
-- TOC entry 1842 (class 0 OID 55677)
-- Dependencies: 1520
-- Data for Name: tbl_SCHEMA; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_SCHEMA" ("ID", "NAME", "NICE_NAME") FROM stdin;
1	sim	\N
\.


--
-- TOC entry 1843 (class 0 OID 55683)
-- Dependencies: 1521
-- Data for Name: tbl_TABLE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_TABLE" ("ID", "INHERITS", "SCHEMA", "NAME", "INHERIT_TYPE", "GENERATE_STORABLE", "INHERITS_IS_USER_DEFINED", "NICE_NAME") FROM stdin;
7	27	1	tblConcentrators	JOINED	t	t	concentrator
6	27	1	tblChannels	JOINED	t	t	channel
9	\N	1	tblCtrlLinks	\N	t	f	ctrl link
22	\N	1	tblModems	\N	t	f	modem
8	\N	1	tblCtrlDeviceTypes	\N	t	f	ctrl device type
21	\N	1	tblModemPools	\N	t	f	modem pool
20	\N	1	tblModemAttributes	\N	t	f	modem attribute
18	\N	1	tblIPAddresses	\N	t	f	ip address
15	\N	1	tblDigDeviceTypes	\N	t	f	dig device type
14	\N	1	tblDigClasses	\N	t	f	dig class
23	\N	1	tblPlant	\N	t	f	plant
24	\N	1	tblPlantElement	\N	t	f	plant element
25	\N	1	tblProtocols	\N	t	f	protocol
27	\N	1	tblSimObjects	\N	t	f	object
28	\N	1	tblSimParams	\N	t	f	param
31	\N	1	tblTestResults	\N	t	f	test result
32	\N	1	tblTestsMaster	\N	t	f	test master
4	\N	1	tblAnaProfiles	\N	t	f	ana profile
3	\N	1	tblAnaDeviceTypes	\N	t	f	ana device type
5	30	1	tblAnaPts	JOINED	t	t	ana pt
2	30	1	tblAccPts	JOINED	t	t	acc pt
19	\N	1	tblLinkedPts	\N	t	f	linked pt
30	27	1	tblTelePts	JOINED	t	t	tele pt
29	27	1	tblStrPts	JOINED	t	t	str pt
26	27	1	tblRtus	JOINED	t	t	rtu
16	30	1	tblDigPts	JOINED	t	t	dig pt
13	27	1	tblDataPorts	JOINED	t	t	data port
12	27	1	tblDataGroups	JOINED	t	t	data group
11	27	1	tblDDNBlocks	JOINED	t	t	ddn block
10	30	1	tblCtrlPts	JOINED	t	t	ctrl pt
266	\N	1	tblDragDropLookup	\N	t	f	drag drop lookup
\.


--
-- TOC entry 1844 (class 0 OID 55691)
-- Dependencies: 1522
-- Data for Name: tbl_VALUE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_VALUE" ("ID", "VALUE_TYPE", "NAME", "TOOLTIP", "HELP") FROM stdin;
\.


--
-- TOC entry 1845 (class 0 OID 55698)
-- Dependencies: 1523
-- Data for Name: tbl_VALUE_TYPE; Type: TABLE DATA; Schema: sams_meta; Owner: postgres
--

COPY "tbl_VALUE_TYPE" ("ID", "NAME", "COMMENT", "GENERATE_ENUM", "TABLE") FROM stdin;
\.


--
-- TOC entry 1813 (class 2606 OID 55707)
-- Dependencies: 1518 1518
-- Name: tbl_COLUMN_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1815 (class 2606 OID 55709)
-- Dependencies: 1519 1519 1519
-- Name: tbl_OBJECT_TYPE_INHERITS_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_INHERITS_NAME_key" UNIQUE ("INHERITS", "NAME");


--
-- TOC entry 1817 (class 2606 OID 55711)
-- Dependencies: 1519 1519
-- Name: tbl_OBJECT_TYPE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1819 (class 2606 OID 55713)
-- Dependencies: 1520 1520
-- Name: tbl_SCHEMA_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_SCHEMA"
    ADD CONSTRAINT "tbl_SCHEMA_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1821 (class 2606 OID 55715)
-- Dependencies: 1521 1521
-- Name: tbl_TABLE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1827 (class 2606 OID 55717)
-- Dependencies: 1523 1523
-- Name: tbl_VALUE_TYPE_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_NAME_key" UNIQUE ("NAME");


--
-- TOC entry 1829 (class 2606 OID 55719)
-- Dependencies: 1523 1523
-- Name: tbl_VALUE_TYPE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1823 (class 2606 OID 55721)
-- Dependencies: 1522 1522 1522
-- Name: tbl_VALUE_VALUE_TYPE_NAME_key; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_VALUE_TYPE_NAME_key" UNIQUE ("VALUE_TYPE", "NAME");


--
-- TOC entry 1825 (class 2606 OID 55723)
-- Dependencies: 1522 1522
-- Name: tbl_VALUE_pkey; Type: CONSTRAINT; Schema: sams_meta; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 1830 (class 2606 OID 55724)
-- Dependencies: 1518 1812 1518
-- Name: tbl_COLUMN_RELATES_TO_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_RELATES_TO_fkey" FOREIGN KEY ("RELATES_TO") REFERENCES "tbl_COLUMN"("ID") ON DELETE SET NULL;


--
-- TOC entry 1831 (class 2606 OID 55729)
-- Dependencies: 1820 1521 1518
-- Name: tbl_COLUMN_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_TABLE_fkey" FOREIGN KEY ("TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1832 (class 2606 OID 55734)
-- Dependencies: 1523 1828 1518
-- Name: tbl_COLUMN_VALUE_TYPE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_COLUMN"
    ADD CONSTRAINT "tbl_COLUMN_VALUE_TYPE_fkey" FOREIGN KEY ("VALUE_TYPE") REFERENCES "tbl_VALUE_TYPE"("ID") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1833 (class 2606 OID 55739)
-- Dependencies: 1816 1519 1519
-- Name: tbl_OBJECT_TYPE_INHERITS_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_INHERITS_fkey" FOREIGN KEY ("INHERITS") REFERENCES "tbl_OBJECT_TYPE"("ID") ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 1834 (class 2606 OID 55744)
-- Dependencies: 1519 1521 1820
-- Name: tbl_OBJECT_TYPE_OBJECT_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_OBJECT_TYPE_TABLE_fkey" FOREIGN KEY ("OBJECT_TYPE_TABLE") REFERENCES "tbl_TABLE"("ID");


--
-- TOC entry 1835 (class 2606 OID 55749)
-- Dependencies: 1521 1519 1820
-- Name: tbl_OBJECT_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_OBJECT_TYPE"
    ADD CONSTRAINT "tbl_OBJECT_TYPE_TABLE_fkey" FOREIGN KEY ("DATA_TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE RESTRICT ON DELETE SET NULL;


--
-- TOC entry 1836 (class 2606 OID 55754)
-- Dependencies: 1820 1521 1521
-- Name: tbl_TABLE_INHERITS_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_INHERITS_fkey" FOREIGN KEY ("INHERITS") REFERENCES "tbl_TABLE"("ID") ON DELETE CASCADE;


--
-- TOC entry 1837 (class 2606 OID 55759)
-- Dependencies: 1520 1521 1818
-- Name: tbl_TABLE_SCHEMA_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_TABLE"
    ADD CONSTRAINT "tbl_TABLE_SCHEMA_fkey" FOREIGN KEY ("SCHEMA") REFERENCES "tbl_SCHEMA"("ID") ON DELETE CASCADE;


--
-- TOC entry 1839 (class 2606 OID 55764)
-- Dependencies: 1820 1523 1521
-- Name: tbl_VALUE_TYPE_TABLE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_VALUE_TYPE"
    ADD CONSTRAINT "tbl_VALUE_TYPE_TABLE_fkey" FOREIGN KEY ("TABLE") REFERENCES "tbl_TABLE"("ID") ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 1838 (class 2606 OID 55769)
-- Dependencies: 1523 1828 1522
-- Name: tbl_VALUE_VALUE_TYPE_fkey; Type: FK CONSTRAINT; Schema: sams_meta; Owner: postgres
--

ALTER TABLE ONLY "tbl_VALUE"
    ADD CONSTRAINT "tbl_VALUE_VALUE_TYPE_fkey" FOREIGN KEY ("VALUE_TYPE") REFERENCES "tbl_VALUE_TYPE"("ID") ON UPDATE RESTRICT ON DELETE CASCADE;


-- Completed on 2015-07-05 16:27:07

--
-- PostgreSQL database dump complete
--

